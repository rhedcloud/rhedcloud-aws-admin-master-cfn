AWSTemplateFormatVersion: 2010-09-09
Description: RHEDcloud AWS CloudFormation Template for Beanstalk IAM roles
Parameters:
  BeanstalkAppsBucket:
    Description: S3 Bucket to create to store the application packages for Elastic Beanstalk. Must be unique.
    Type: String
    AllowedPattern: '(?=^.{3,63}$)(?!^(\d+\.)+\d+$)(^(([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])\.)*([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])$)'
    ConstraintDescription: Must be a valid s3 bucket name
  BeanstalkAppsConfigBucket:
    Description: S3 Bucket to create to store the application packages for Elastic Beanstalk. Must be unique.
    Type: String
    AllowedPattern: '(?=^.{3,63}$)(?!^(\d+\.)+\d+$)(^(([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])\.)*([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])$)'
    ConstraintDescription: Must be a valid s3 bucket name
Resources:
  ServiceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: RHEDcloud-elasticbeanstalk-role
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Sid: ''
            Effect: Allow
            Principal:
              Service: elasticbeanstalk.amazonaws.com
            Action: 'sts:AssumeRole'
            Condition:
              StringEquals:
                'sts:ExternalId': elasticbeanstalk
      ManagedPolicyArns:
       - "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
       - "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth"
      Path: /
  InstanceProfile:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      InstanceProfileName: RHEDcloud-elasticbeanstalk-ec2-role
      Path: /
      Roles:
        - !Ref InstanceProfileRole
  InstanceProfileRole:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: RHEDcloud-elasticbeanstalk-ec2-role
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Policies:
        - PolicyName: root
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Sid: BucketAccess
                Action:
                  - 's3:Get*'
                  - 's3:List*'
                  - 's3:PutObject'
                Effect: Allow
                Resource:
                  - !Sub
                    - 'arn:aws:s3:::${BeanstalkAppsBucket}'
                    - { BeanstalkAppsBucket: !Ref 'BeanstalkAppsBucket' }
                  - !Sub
                    - 'arn:aws:s3:::${BeanstalkAppsBucket}/*'
                    - { BeanstalkAppsBucket: !Ref 'BeanstalkAppsBucket' }
      ManagedPolicyArns:
       - "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
       - "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
       - "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
      Path: /
  DynamoDbScalingRole:
   Type: "AWS::IAM::Role"
   Properties:
    RoleName: RHEDcloud-dynamodb-scaling-role
    AssumeRolePolicyDocument:
     Version: "2012-10-17"
     Statement:
      -
       Effect: Allow
       Principal:
        Service:
         -
          "application-autoscaling.amazonaws.com"
       Action:
        -
         "sts:AssumeRole"
    Path: "/"
    Policies:
     -
      PolicyName: root
      PolicyDocument:
       Version: "2012-10-17"
       Statement:
        -
         Effect: Allow
         Action:
          - "dynamodb:DescribeTable"
          - "dynamodb:UpdateTable"
          - "cloudwatch:PutMetricAlarm"
          - "cloudwatch:DescribeAlarms"
          - "cloudwatch:GetMetricStatistics"
          - "cloudwatch:SetAlarmState"
          - "cloudwatch:DeleteAlarms"
         Resource: "*"
  RHEDcloudArtifactUser:
   Type: AWS::IAM::User
   Properties:
     Path: /
     Policies:
     - PolicyName: RHEDcloudArtifactBucketAccess
       PolicyDocument:
         Version: '2012-10-17'
         Statement:
         - Effect: Allow
           Action:
           - s3:Get*
           - s3:List*
           - s3:PutObject
           Resource:
           - !Sub
             - 'arn:aws:s3:::${BeanstalkAppsBucket}'
             - { BeanstalkAppsBucket: !Ref 'BeanstalkAppsBucket' }
           - !Sub
             - 'arn:aws:s3:::${BeanstalkAppsBucket}/*'
             - { BeanstalkAppsBucket: !Ref 'BeanstalkAppsBucket' }
     UserName: artifact-deploy
  RHEDcloudArtifactUserKeys:
     Type: AWS::IAM::AccessKey
     Properties:
       Serial: 1
       Status: Active
       UserName: !Ref RHEDcloudArtifactUser

Outputs:
  EbServiceRole:
    Value: !Ref 'ServiceRole'
    Export:
      Name: RHEDcloud-EbServiceRole
  EbInstanceProfile:
    Value: !Ref 'InstanceProfile'
    Export:
      Name: RHEDcloud-EbInstanceProfile
  DynamoDbScalingRoleArn:
    Value: !GetAtt 'DynamoDbScalingRole.Arn'
    Export:
      Name: RHEDcloud-DynamoDbScalingRoleArn
  RHEDcloudArtifactUser:
    Value: !Ref 'RHEDcloudArtifactUser'
    Export:
      Name: RHEDcloud-RHEDcloudArtifactUser
  RHEDcloudArtifactUserAccessKeyId:
    Value: !Ref 'RHEDcloudArtifactUserKeys'
    Export:
      Name: RHEDcloud-RHEDcloudArtifactUserAccessKeyId
  RHEDcloudArtifactUserSecretAccessKey:
    Value: !GetAtt 'RHEDcloudArtifactUserKeys.SecretAccessKey'
    Export:
      Name: RHEDcloud-RHEDcloudArtifactUserSecretAccessKey
