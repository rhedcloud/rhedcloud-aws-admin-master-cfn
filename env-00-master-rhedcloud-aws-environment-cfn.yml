AWSTemplateFormatVersion: 2010-09-09
Description: Master Stack to deploy RHEDcloud Environments
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: RHEDcloud General Settings
      Parameters:
        - AcmCertificateArn
        - Ec2KeyPair
    - Label:
        default: RHEDcloud Environment Settings
      Parameters:
        - Environment
        - CNAMEPrefix
        - DocUriBaseBucket
        - Ec2InstanceType
        - DeployMq
        - MqHostInstanceType
        - MqDeploymentMode
        - MqBrokerUser
        - MqBrokerPassword
        - MqAwsAccountServicePassword
        - MqIdmServicePassword
        - MqEmailAddressValidationServicePassword
        - MqTkiServicePassword
        - MqSrdServicePassword
        - MqNetworkOpsServicePassword
        - MqCiscoAsrServicePassword
        - DynamoReadCapacityUnits
        - DynamoWriteCapacityUnits
        - DeployMysqlAurora
        - DeployOracle
        - DeployPostgresql
        - DatabaseInstanceType
        - MultiAZDatabase
        - DatabaseMasterPassword
    ParameterLabels:
      CNAMEPrefix:
        default: Prefix to use for ALB cnames ie emory, surge, rhedcloud
      Environment:
        default: Name of environment ie dev, stage, test, prod
      DocUriBaseBucket:
        default: Name of bucket for DocUriBase ie rhedcloud-config-dev
      Ec2KeyPair:
        default: EC2 Key Pair
      AcmCertificateArn:
        default: AWS ACM Certificate ARN
      Ec2InstanceType:
        default: AWS Ec2 Instance type to use for application stacks
      DeployMq:
        default: Deploy AWS MQ for this environment
      MqHostInstanceType:
        default: AWS MQ Instance Type
      MqDeploymentMode:
        default: AWS MQ Deployment Mode
      MqBrokerUser:
        default: AWS MQ Broker Admin user
      MqBrokerPassword:
        default: AWS MQ Broker Admin password
      MqAwsAccountServicePassword:
        default: AWS MQ AWS Account Service password
      MqIdmServicePassword:
        default: AWS MQ Identity Management Service password
      MqEmailAddressValidationServicePassword:
        default: AWS MQ Email Address Validation Service password
      MqTkiServicePassword:
        default: AWS MQ TKI Service password
      MqSrdServicePassword:
        default: AWS MQ SRD Service password
      MqNetworkOpsServicePassword:
        default: AWS MQ Network Ops Service password
      MqCiscoAsrServicePassword:
        default: AWS MQ Cisco ASR Service password
      DynamoReadCapacityUnits:
        default: DynamoDB Initial Capacity Units
      DynamoWriteCapacityUnits:
        default: DynamoDB Initial Capacity Units
      DatabaseInstanceType:
        default: Database Instance Type
      MultiAZDatabase:
        default: Multi AZ (Only affects Aurora Mysql currently)
      DatabaseMasterPassword:
        default: RDS Master Database password
      DeployMysqlAurora:
        default: Deploy Mysql Aurora RDS for this environment
      DeployPostgresql:
        default: Deploy Postgresql RDS for this environment
      DeployOracle:
        default: Deploy Oracle RDS for this environment
Parameters:
  Environment:
    Description: "[ REQUIRED ] Only letters allowed, max 5 characters ie dev stage test prod"
    Type: String
    MinLength: 2
    MaxLength: 5
    AllowedPattern: "^[a-zA-Z]*$"
  CNAMEPrefix:
    Description: "[ REQUIRED ] Only letters allowed, max 10 characters ie surge, emory, rhedcloud"
    Type: String
    MinLength: 3
    MaxLength: 10
    AllowedPattern: "^[a-zA-Z]*$"
  DocUriBaseBucket:
    Description: '[ REQUIRED ] S3 Bucket to store docUri files. Must be unique.'
    Type: String
    AllowedPattern: '(?=^.{3,63}$)(?!^(\d+\.)+\d+$)(^(([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])\.)*([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])$)'
    Default: "orgname-rhedcloud-config-dev"
    ConstraintDescription: Must be a valid s3 bucket name
  AcmCertificateArn:
    Description: "[ REQUIRED ] Valid AWS Certificate Manager ARN to use on Load Balancers for SSL"
    Type: String
    Default: ""
  Ec2KeyPair:
    Description: "[ REQUIRED ] Please create one in the Ec2 console or import."
    Type: AWS::EC2::KeyPair::KeyName
  Ec2InstanceType:
    Description: The Amazon EC2 instance type for your development web instances.
    Default: t3.small
    ConstraintDescription: Must be a valid Amazon EC2 instance type.
    Type: String
    AllowedValues:
      - t2.small
      - t2.medium
      - t2.large
      - t3.small
      - t3.medium
      - t3.large
      - m5.large
  # MQ Parameters
  DeployMq:
    Description: Deploy MQ for this Environment
    Type: String
    AllowedValues:
      - true
      - false
    Default: true
  MqHostInstanceType:
    Description: MQ Instance type to use. Recommended mq.m5.large
    Type: String
    Default: "mq.m5.large"
    AllowedValues:
      - mq.m5.large
      - mq.m5.xlarge
      - mq.m5.2xlarge
      - mq.m5.4xlarge
  MqDeploymentMode:
    Type: String
    Default: 'SINGLE_INSTANCE'
    AllowedValues:
      - SINGLE_INSTANCE
      - ACTIVE_STANDBY_MULTI_AZ
  MqBrokerUser:
    Type: String
    Default: activemq
  MqBrokerPassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  MqAwsAccountServicePassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  MqIdmServicePassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  MqEmailAddressValidationServicePassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  MqTkiServicePassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  MqSrdServicePassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  MqNetworkOpsServicePassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  MqCiscoAsrServicePassword:
    Description: The password to access the Amazon MQ broker. Min 12 characters
    Type: String
    MinLength: 12
    ConstraintDescription: The Amazon MQ broker password is required !
    NoEcho: true
  # DynamoDB Parameters
  DynamoReadCapacityUnits:
    Description: Provisioned initial read throughput
    Type: Number
    Default: '5'
    MinValue: '5'
    MaxValue: '10000'
    ConstraintDescription: must be between 5 and 10000
  DynamoWriteCapacityUnits:
    Description: Provisioned initial write throughput
    Type: Number
    Default: '5'
    MinValue: '5'
    MaxValue: '10000'
    ConstraintDescription: must be between 5 and 10000
  # RDS Parameters
  DeployMysqlAurora:
    Type: String
    AllowedValues:
      - true
      - false
    Default: false
  DeployPostgresql:
    Type: String
    AllowedValues:
      - true
      - false
    Default: true
  DeployOracle:
    Type: String
    AllowedValues:
      - true
      - false
    Default: false
  DatabaseInstanceType:
    AllowedValues:
      - db.t2.small
      - db.t2.medium
      - db.t3.small
      - db.t3.medium
      - db.r4.large
      - db.r4.xlarge
      - db.r4.2xlarge
      - db.r4.4xlarge
      - db.r4.8xlarge
      - db.r4.16xlarge
      - db.r5.large
      - db.r5.xlarge
      - db.r5.2xlarge
      - db.r5.4xlarge
      - db.r5.8xlarge
      - db.r5.16xlarge
    ConstraintDescription: Must be a valid RDS instance class.
    Default: db.t3.small
    Description: The Amazon RDS database instance class.
    Type: String
  MultiAZDatabase:
    AllowedValues:
      - true
      - false
    Default: false
    Description: Specifies whether a to deploy the AWS Aurora MySQL Database in Multi-AZ configuration.
    Type: String
  DatabaseMasterPassword:
    Description: Must be letters (upper or lower), numbers, spaces, and these special characters `~#$%^&*()_+,-
    Type: String
    NoEcho: true
    AllowedPattern: ^([a-zA-Z0-9`~#$%^&*()_+,\\-])*$
    ConstraintDescription: The Amazon RDS master password. Letters, numbers, spaces, and these special characters `~#$%^&*()_+,-
Conditions:
  DeployMq: !Equals [ true, !Ref 'DeployMq' ]
Resources:
  RHEDcloudMq:
    Type: AWS::CloudFormation::Stack
    Condition: DeployMq
    Properties:
      Parameters:
        VpcId: !ImportValue 'RHEDcloud-VpcId'
        VpcCidr: !ImportValue 'RHEDcloud-VpcCidr'
        PrivateSubnet1: !ImportValue 'RHEDcloud-PrivateSubnet1Id'
        PrivateSubnet2: !ImportValue 'RHEDcloud-PrivateSubnet2Id'
        Environment:
          Fn::Transform:
            - Name: String
              Parameters:
                InputString: !Ref "Environment"
                Operation: Upper
        MqHostInstanceType: !Ref 'MqHostInstanceType'
        MqDeploymentMode: !Ref 'MqDeploymentMode'
        MqBrokerUser: !Ref 'MqBrokerUser'
        MqBrokerPassword: !Ref 'MqBrokerPassword'
        MqAwsAccountServicePassword: !Ref 'MqAwsAccountServicePassword'
        MqIdmServicePassword: !Ref 'MqIdmServicePassword'
        MqEmailAddressValidationServicePassword: !Ref 'MqEmailAddressValidationServicePassword'
        MqTkiServicePassword: !Ref 'MqTkiServicePassword'
        MqSrdServicePassword: !Ref 'MqSrdServicePassword'
        MqNetworkOpsServicePassword: !Ref 'MqNetworkOpsServicePassword'
        MqCiscoAsrServicePassword: !Ref 'MqCiscoAsrServicePassword'
      TemplateURL: !Sub
        - 'https://s3.amazonaws.com/${CfnTemplateBucket}/env-01-rhedcloud-aws-mq-cfn.yml'
        - { CfnTemplateBucket: !ImportValue 'RHEDcloud-CfnTemplateBucket' }
  RHEDcloudDynamoDb:
    Type: AWS::CloudFormation::Stack
    Properties:
      Parameters:
        Environment:
          Fn::Transform:
            - Name: String
              Parameters:
                InputString: !Ref "Environment"
                Operation: Capitalize
        ScalingRole: !ImportValue 'RHEDcloud-DynamoDbScalingRoleArn'
        DynamoReadCapacityUnits: !Ref 'DynamoReadCapacityUnits'
        DynamoWriteCapacityUnits: !Ref 'DynamoWriteCapacityUnits'
      TemplateURL: !Sub
        - 'https://s3.amazonaws.com/${CfnTemplateBucket}/env-01-rhedcloud-aws-dynamodb-cfn.yml'
        - { CfnTemplateBucket: !ImportValue 'RHEDcloud-CfnTemplateBucket' }
  RHEDcloudDocUriBucket:
    Type: AWS::CloudFormation::Stack
    Properties:
      Parameters:
        DocUriBaseBucket: !Ref 'DocUriBaseBucket'
      TemplateURL: !Sub
        - 'https://s3.amazonaws.com/${CfnTemplateBucket}/env-01-rhedcloud-aws-s3-cfn.yml'
        - { CfnTemplateBucket: !ImportValue 'RHEDcloud-CfnTemplateBucket' }
  RHEDcloudRdsDb:
    Type: AWS::CloudFormation::Stack
    Properties:
      Parameters:
        VpcId: !ImportValue 'RHEDcloud-VpcId'
        VpcCidr: !ImportValue 'RHEDcloud-VpcCidr'
        PrivateSubnet1: !ImportValue 'RHEDcloud-PrivateSubnet1Id'
        PrivateSubnet2: !ImportValue 'RHEDcloud-PrivateSubnet2Id'
        Environment: !Ref 'Environment'
        DeployMysqlAurora: !Ref 'DeployMysqlAurora'
        DeployPostgresql: !Ref 'DeployPostgresql'
        DeployOracle: !Ref 'DeployOracle'
        DatabaseInstanceType: !Ref 'DatabaseInstanceType'
        MultiAZDatabase: !Ref 'MultiAZDatabase'
        DatabaseMasterPassword: !Ref 'DatabaseMasterPassword'
      TemplateURL: !Sub
        - 'https://s3.amazonaws.com/${CfnTemplateBucket}/env-01-rhedcloud-aws-rds-cfn.yml'
        - { CfnTemplateBucket: !ImportValue 'RHEDcloud-CfnTemplateBucket' }
  RHEDcloudBeanstalkEnvironments:
    Type: AWS::CloudFormation::Stack
    Properties:
      Parameters:
        VpcId: !ImportValue 'RHEDcloud-VpcId'
        PublicSubnet1: !ImportValue 'RHEDcloud-PublicSubnet1Id'
        PublicSubnet2: !ImportValue 'RHEDcloud-PublicSubnet2Id'
        PrivateSubnet1: !ImportValue 'RHEDcloud-PrivateSubnet1Id'
        PrivateSubnet2: !ImportValue 'RHEDcloud-PrivateSubnet2Id'
        DeployCiscoAsrService: !ImportValue 'RHEDcloud-DeployCiscoAsrService'
        DeployElasticIpService: !ImportValue 'RHEDcloud-DeployElasticIpService'
        DeployFirewallService: !ImportValue 'RHEDcloud-DeployFirewallService'
        DeployNetworkOperationsService: !ImportValue 'RHEDcloud-DeployNetworkOperationsService'
        DeployServiceNowService: !ImportValue 'RHEDcloud-DeployServiceNowService'
        Environment:
          Fn::Transform:
            - Name: String
              Parameters:
                InputString: !Ref "Environment"
                Operation: Lower
        CNAMEPrefix:
          Fn::Transform:
            - Name: String
              Parameters:
                InputString: !Ref "CNAMEPrefix"
                Operation: Lower
        Ec2KeyPair: !Ref 'Ec2KeyPair'
        Ec2InstanceType: !Ref 'Ec2InstanceType'
        EbServiceRole: !ImportValue 'RHEDcloud-EbServiceRole'
        EbInstanceProfile: !ImportValue 'RHEDcloud-EbInstanceProfile'
        AcmCertificateArn: !Ref 'AcmCertificateArn'
        DocUriBase: !Sub
          - 'https://${DocUriBaseBucket}.s3.amazonaws.com/'
          - { DocUriBaseBucket: !Ref DocUriBaseBucket }
      TemplateURL: !Sub
        - 'https://s3.amazonaws.com/${CfnTemplateBucket}/env-01-rhedcloud-aws-beanstalk-cfn.yml'
        - { CfnTemplateBucket: !ImportValue 'RHEDcloud-CfnTemplateBucket' }
