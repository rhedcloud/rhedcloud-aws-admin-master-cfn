#!/bin/bash

echo copy/edit this template script to README-env-00-site.sh
echo chmod u+x README-env-00-site.sh
echo mkdir rhedcloud-site-setup
echo cd rhedcloud-site-setup
echo ../README-env-00-site.sh 2>&1 \| tee -a README-env-00-site.log
exit

command -v aws >/dev/null 2>&1 || { echo >&2 "AWS cli tool (aws) required but it's not installed.  Aborting."; exit 1; }
command -v jq >/dev/null 2>&1 || { echo >&2 "jq commandline JSON processor (jq) required but it's not installed.  Aborting."; exit 1; }
set -x

### Start of Variables to edit ###

AWS_ADMIN_ACT=admin_aws_account_id_goes_here

# AWS Region to operate in
export AWS_DEFAULT_REGION=us-east-1

# ENVIRONMENTS is an array of Environments to loop through
ENVIRONMENTS=(
  "DEV"
  "TEST"
  "STAGE"
  "PROD"
)

### End of Variables to edit ###

date

for E in "${ENVIRONMENTS[@]}" ; do
    Elc=$(echo $E | tr '[:upper:]' '[:lower:]')
    Eh=$Elc

    case $E in
      DEV)
        MASTER_ACT=dev_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=dev_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=dev_aws_secret_access_key_goes_here
        ;;
      TEST)
        MASTER_ACT=test_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=test_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=test_aws_secret_access_key_goes_here
        ;;
      STAGE)
        MASTER_ACT=stage_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=stage_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=stage_aws_secret_access_key_goes_here
        ;;
      PROD)
        Eh=account
        MASTER_ACT=prod_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=prod_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=prod_aws_secret_access_key_goes_here
        ;;
    esac


    ###############
    ##### Organizations
    aws organizations create-organization --feature-set ALL \
          &>organizations--create-$Elc-master.log

    # Fetch the root_id and organization_id using aws cli and jq
    root_id=$(aws organizations list-roots | jq -r '.Roots[].Id')
    organization_id=$(aws organizations describe-organization | jq -r '.Organization.Id')

    # Enable SCPs for this account
    aws organizations enable-policy-type --root-id $root_id --policy-type SERVICE_CONTROL_POLICY --output text \
          >> organizations--create-$Elc-master.log 2>&1

    # without a sleep we sometimes get ConcurrentModificationException on the next organizations operation
    sleep 10

    # Create OU structure
    (
    aws organizations create-organizational-unit --parent-id $root_id --name RHEDcloudStandardAccountOrg
    aws organizations create-organizational-unit --parent-id $root_id --name RHEDcloudHipaaAccountOrg
    aws organizations create-organizational-unit --parent-id $root_id --name RHEDcloudAccountAdministrationOrg
    aws organizations create-organizational-unit --parent-id $root_id --name RHEDcloudAccountPendingDeleteOrg
    aws organizations create-organizational-unit --parent-id $root_id --name RHEDcloudAccountQuarantineOrg
    aws organizations create-organizational-unit --parent-id $root_id --name RHEDcloudAccountSuspendedOrg
    ) &>organizations--ous-$Elc-master.log


    ###############
    ##### SAML Provider
    (
    aws iam create-saml-provider --name Site_Prod_IDP --saml-metadata-document file://Site_Prod_SAML_metadata_document.xml
    ) &>saml-provider--create--$Elc-master.log

    ###############
    ##### Account Alias
    # account aliases should be prefixed with the name of the institution because they must be globally unique
    aws iam create-account-alias --account-alias site-$Eh-master \
          &>account-alias--create-$Elc-master.log

    ###############
    ##### RHEDcloudTkiService
    (
    aws iam create-user --user-name RHEDcloudTkiService
    aws iam attach-user-policy --user-name RHEDcloudTkiService \
      --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
    aws iam create-access-key --user-name RHEDcloudTkiService > RHEDcloudTkiService--accesskey-$Elc-master
    ) &>RHEDcloudTkiService--user-$Elc-master.log

    ###############
    ##### RHEDcloudVpcpApplication
    (
    aws iam create-user --user-name RHEDcloudVpcpApplication
    aws iam attach-user-policy --user-name RHEDcloudVpcpApplication \
      --policy-arn "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
    aws iam create-access-key --user-name RHEDcloudVpcpApplication > RHEDcloudVpcpApplication--accesskey-$Elc-master
    ) &>RHEDcloudVpcpApplication--user-$Elc-master.log

    ###############
    ##### RHEDcloudSecurityRiskDetectionService

    cat <<EOF > RHEDcloudSecurityRiskDetectionService--policy-1-$Elc-master.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Resource": [
                "arn:aws:iam::*:role/rhedcloud/RHEDcloudSecurityRiskDetectionServiceRole"
            ]
        }
    ]
}
EOF
    cat <<EOF > RHEDcloudSecurityRiskDetectionService--policy-2-$Elc-master.json
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Action": "organizations:*",
        "Resource": "*"
    }
}
EOF
    (
    aws iam create-user --user-name RHEDcloudSecurityRiskDetectionService

    aws iam create-policy \
      --policy-name RHEDcloudSecurityRiskDetectionServiceCrossAccountAccess \
      --policy-document file://RHEDcloudSecurityRiskDetectionService--policy-1-$Elc-master.json \
      --description "This policy provides cross account access for the Security Risk Detection Service."
    aws iam attach-user-policy --user-name RHEDcloudSecurityRiskDetectionService \
      --policy-arn "arn:aws:iam::$MASTER_ACT:policy/RHEDcloudSecurityRiskDetectionServiceCrossAccountAccess"

    aws iam create-policy \
      --policy-name RHEDcloudSecurityRiskDetectionServiceOrganizationsFullAccess \
      --policy-document file://RHEDcloudSecurityRiskDetectionService--policy-2-$Elc-master.json \
      --description "This policy provides full access to AWS Organizations for the Security Risk Detection Service."
    aws iam attach-user-policy --user-name RHEDcloudSecurityRiskDetectionService \
      --policy-arn "arn:aws:iam::$MASTER_ACT:policy/RHEDcloudSecurityRiskDetectionServiceOrganizationsFullAccess"

    aws iam create-access-key --user-name RHEDcloudSecurityRiskDetectionService > RHEDcloudSecurityRiskDetectionService--accesskey-$Elc-master
    ) &>RHEDcloudSecurityRiskDetectionService--user-$Elc-master.log

    ###############
    ##### Central_IT_Administrator

    cat <<EOF > Central_IT_Administrator--policy-$Elc-master.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::*:role/OrganizationAccountAccessRole",
        "arn:aws:iam::*:role/rhedcloud/RHEDcloudMaintenanceOperatorRole"
      ]
    }
  ]
}
EOF
    (
    aws iam create-group --group-name Central_IT_Administrator
    aws iam attach-group-policy --group-name Central_IT_Administrator \
      --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
    aws iam put-group-policy --group-name Central_IT_Administrator \
      --policy-name "Central_IT_Administrator-MemberAccountAccess" \
      --policy-document file://Central_IT_Administrator--policy-$Elc-master.json
    ) &>Central_IT_Administrator--group-$Elc-master.log

    ###############
    ##### RHEDcloudAwsAccountService

    cat <<EOF > RHEDcloudAwsAccountService--policy-$Elc-master.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Resource": [
                "arn:aws:iam::*:role/rhedcloud/RHEDcloudAwsAccountServiceRole"
            ]
        }
    ]
}
EOF
    (
    aws iam create-user --user-name RHEDcloudAwsAccountService

    aws iam create-policy \
      --policy-name RHEDcloudAwsAccountServiceCrossAccountAccess \
      --policy-document file://RHEDcloudAwsAccountService--policy-$Elc-master.json \
      --description "This policy provides cross account access for the AwsAccountService."
    aws iam attach-user-policy --user-name RHEDcloudAwsAccountService \
      --policy-arn "arn:aws:iam::$MASTER_ACT:policy/RHEDcloudAwsAccountServiceCrossAccountAccess"
    aws iam add-user-to-group --user-name RHEDcloudAwsAccountService --group-name Central_IT_Administrator

    aws iam create-access-key --user-name RHEDcloudAwsAccountService > RHEDcloudAwsAccountService--accesskey-$Elc-master
    ) &>RHEDcloudAwsAccountService--user-$Elc-master.log

    ###############
    ##### RHEDcloudMaintenanceOperatorRole

    cat <<EOF > RHEDcloudMaintenanceOperatorRole--policy-$Elc-master.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$MASTER_ACT:saml-provider/Site_Prod_IDP"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "saml:aud": "https://signin.aws.amazon.com/saml",
          "saml:iss": "https://idp.site.org/idp/shibboleth"
        }
      }
    }
  ]
}
EOF
    (
    aws iam create-role --path '/rhedcloud/' \
      --role-name RHEDcloudMaintenanceOperatorRole \
      --assume-role-policy-document file://RHEDcloudMaintenanceOperatorRole--policy-$Elc-master.json \
      --max-session-duration 43200
    aws iam attach-role-policy --role-name RHEDcloudMaintenanceOperatorRole \
      --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
    ) &>RHEDcloudMaintenanceOperatorRole--create-$Elc-master.log

    ###############
    ##### RHEDcloudAdministratorRole

    cat <<EOF > RHEDcloudAdministratorRole--policy-$Elc-master.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$MASTER_ACT:saml-provider/Site_Prod_IDP"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "saml:aud": "https://signin.aws.amazon.com/saml",
          "saml:iss": "https://idp.site.org/idp/shibboleth"
        }
      }
    }
  ]
}
EOF
    (
    aws iam create-role --path '/rhedcloud/' \
      --role-name RHEDcloudAdministratorRole \
      --description 'RHEDcloud Administrator Role for Customer Administrators' \
      --assume-role-policy-document file://RHEDcloudAdministratorRole--policy-$Elc-master.json \
      --max-session-duration 3600
    aws iam attach-role-policy --role-name RHEDcloudAdministratorRole \
      --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
    ) &>RHEDcloudAdministratorRole--create-$Elc-master.log

    ###############
    ##### RHEDcloudCentralAdministratorRole

    cat <<EOF > RHEDcloudCentralAdministratorRole--policy-$Elc-master.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$MASTER_ACT:saml-provider/Site_Prod_IDP"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "saml:aud": "https://signin.aws.amazon.com/saml",
          "saml:iss": "https://idp.site.org/idp/shibboleth"
        }
      }
    }
  ]
}
EOF
    (
    aws iam create-role --path '/rhedcloud/' \
      --role-name RHEDcloudCentralAdministratorRole \
      --description 'RHEDcloud Central Administrator Role for Central IT Admins' \
      --assume-role-policy-document file://RHEDcloudCentralAdministratorRole--policy-$Elc-master.json \
      --max-session-duration 3600
    aws iam attach-role-policy --role-name RHEDcloudCentralAdministratorRole \
      --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
    ) &>RHEDcloudCentralAdministratorRole--create-$Elc-master.log

    ###############
    ##### RHEDcloudAuditorRole

    cat <<EOF > RHEDcloudAuditorRole--policy-$Elc-master.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$MASTER_ACT:saml-provider/Site_Prod_IDP"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "saml:aud": "https://signin.aws.amazon.com/saml",
          "saml:iss": "https://idp.site.org/idp/shibboleth"
        }
      }
    }
  ]
}
EOF
    (
    aws iam create-role --path '/rhedcloud/' \
      --role-name RHEDcloudAuditorRole \
      --description 'RHEDcloud Role for Auditors' \
      --assume-role-policy-document file://RHEDcloudAuditorRole--policy-$Elc-master.json \
      --max-session-duration 3600
    aws iam attach-role-policy --role-name RHEDcloudAuditorRole \
      --policy-arn "arn:aws:iam::aws:policy/job-function/ViewOnlyAccess"
    ) &>RHEDcloudAuditorRole--create-$Elc-master.log

    ###############
    ##### wait for the user creation to reach consistency
    sleep 20

    ###############
    ##### TKI CLI Client Installer bucket
    # bucket name should be prefixed with the name of the institution because bucket names must be globally unique

    BUCKET_NAME=site-rhedcloud-aws-$Elc-tki-client

    cat <<EOF > tki-installer-bucket--policy-$Elc-master.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "DistributeTkiCliClient",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::$AWS_ADMIN_ACT:user/artifact-deploy"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::$BUCKET_NAME/*"
        },
        {
            "Sid": "DistributeTkiCliClientConfirm",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::$AWS_ADMIN_ACT:user/artifact-deploy"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::$BUCKET_NAME"
        },
        {
            "Sid": "TkiClientInstallerDownloadsForVpcp",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::$MASTER_ACT:user/RHEDcloudVpcpApplication"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::$BUCKET_NAME/*"
        },
        {
            "Sid": "TkiClientInstallerDownloads",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::$AWS_ADMIN_ACT:user/tki-client-$Elc"
            },
            "Action": "s3:GetObject",
            "Resource": [
                "arn:aws:s3:::$BUCKET_NAME/VERSION*.txt",
                "arn:aws:s3:::$BUCKET_NAME/linux-$Elc-installer.tar.gz",
                "arn:aws:s3:::$BUCKET_NAME/mac-$Elc-installer.tar.gz",
                "arn:aws:s3:::$BUCKET_NAME/windows-$Elc-installer.zip"
            ]
        }
    ]
}
EOF
    (
    aws s3api create-bucket --bucket $BUCKET_NAME
    aws s3api put-bucket-policy --bucket $BUCKET_NAME --policy file://tki-installer-bucket--policy-$Elc-master.json
    aws s3api put-bucket-versioning --bucket $BUCKET_NAME --versioning-configuration Status=Enabled
    ) &>tki-installer-bucket--create-$Elc-master.log


    ###############
    ##### CFN Templates bucket
    # bucket name should be prefixed with the name of the institution because bucket names must be globally unique
    # for some background, see https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_condition-keys.html#condition-keys-principalorgid

    BUCKET_NAME=site-rhedcloud-aws-$Elc-scps

    cat <<EOF > cfn-template1-bucket--policy-$Elc-master.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::$AWS_ADMIN_ACT:user/artifact-deploy"
                ]
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::$BUCKET_NAME/*"
        },
        {
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::$BUCKET_NAME/*",
            "Condition": {
                "ForAllValues:StringNotLike": {
                    "aws:PrincipalOrgID": [
                        "$organization_id"
                    ]
                }
            }
        }
    ]
}
EOF
    (
    aws s3api create-bucket --bucket $BUCKET_NAME
    aws s3api put-bucket-policy --bucket $BUCKET_NAME --policy file://cfn-template1-bucket--policy-$Elc-master.json
    aws s3api put-public-access-block --bucket $BUCKET_NAME --public-access-block-configuration 'BlockPublicAcls=false,IgnorePublicAcls=false,BlockPublicPolicy=false,RestrictPublicBuckets=false'
    ) &>cfn-template1-bucket--create-$Elc-master.log


    BUCKET_NAME=site-rhedcloud-aws-$Elc-cfn-templates

    cat <<EOF > cfn-template2-bucket--policy-$Elc-master.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::$AWS_ADMIN_ACT:user/artifact-deploy"
                ]
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::$BUCKET_NAME/*"
        },
        {
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::$BUCKET_NAME/*",
            "Condition": {
                "ForAllValues:StringNotLike": {
                    "aws:PrincipalOrgID": [
                        "$organization_id"
                    ]
                }
            }
        }
    ]
}
EOF
    (
    aws s3api create-bucket --bucket $BUCKET_NAME
    aws s3api put-bucket-policy --bucket $BUCKET_NAME --policy file://cfn-template2-bucket--policy-$Elc-master.json
    aws s3api put-public-access-block --bucket $BUCKET_NAME --public-access-block-configuration 'BlockPublicAcls=false,IgnorePublicAcls=false,BlockPublicPolicy=false,RestrictPublicBuckets=false'
    ) &>cfn-template2-bucket--create-$Elc-master.log


    ###############
    ##### Bucket OWNER_ID is needed by the deploy-only pipelines
    # Needed by RHEDcloud AWS Org Standard/HIPAA SCPs,
    #       and RHEDcloud AWS VPC Type1 CloudFormation templates,
    #       and RHEDcloud AWS RS Account CloudFormation template

    (
    aws s3api list-buckets
    ) &>bucket-owner--list-$Elc-master.log

done
