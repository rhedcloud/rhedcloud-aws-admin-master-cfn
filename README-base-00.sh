#!/bin/bash

echo copy/edit this template script to README-base-00-site.sh
echo chmod u+x README-base-00-site.sh
echo mkdir rhedcloud-site-setup
echo cd rhedcloud-site-setup
echo ../README-base-00-site.sh 2>&1 \| tee -a README-base-00-site.log
exit

command -v aws >/dev/null 2>&1 || { echo >&2 "AWS cli tool (aws) required but it's not installed.  Aborting."; exit 1; }
command -v openssl >/dev/null 2>&1 || { echo >&2 "OpenSSL required but it's not installed.  Aborting."; exit 1; }
command -v psql >/dev/null 2>&1 || { echo >&2 "PostgreSQL client required but it's not installed.  Aborting."; exit 1; }
set -x

### Start of Variables to edit ###

AWS_ADMIN_ACT=admin_aws_account_id_goes_here
export AWS_ACCESS_KEY_ID=admin_aws_access_key_id_goes_here
export AWS_SECRET_ACCESS_KEY=admin_aws_secret_access_key_goes_here

# AWS Region to operate in
export AWS_DEFAULT_REGION=us-east-1

# password for the postgres user
POSTGRES_USER_PASSWORD=postgres_user_password_goes_here

# ENVIRONMENTS is an array of Environments to loop through
ENVIRONMENTS=(
  "DEV"
  "TEST"
  "STAGE"
  "PROD"
)

### End of Variables to edit ###

date

###############
##### Organizations
# the admin account will be the top of an Organizations hierarchy

aws organizations create-organization --feature-set ALL \
      &>organizations--create-admin.log

###############
##### SAML Provider
# the Site_Prod_SAML_metadata_document.xml file is used in multiple places
(
curl -o Site_Prod_SAML_metadata_document.xml https://idp.site.org/idp/shibboleth
aws iam create-saml-provider --name Site_Prod_IDP --saml-metadata-document file://Site_Prod_SAML_metadata_document.xml
) &>saml-provider--create-admin.log

###############
##### Account Alias
# account aliases should be prefixed with the name of the institution because they must be globally unique
aws iam create-account-alias --account-alias site-admin-1 \
      &>account-alias--create-admin.log


for E in "${ENVIRONMENTS[@]}" ; do
    Elc=$(echo $E | tr '[:upper:]' '[:lower:]')
    Ecc="$(tr '[:lower:]' '[:upper:]' <<< ${Elc:0:1})${Elc:1}"


    ###############
    ##### database structures
    (
    P=$(openssl rand -base64 20)

    echo $E,$P > rhedcloud-db--role-password-$Elc.csv
    export PGPASSWORD=$POSTGRES_USER_PASSWORD
    psql -h 127.0.0.1 postgres postgres <<EOF
CREATE ROLE rhedcloud_$Elc WITH LOGIN PASSWORD '$P' SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;

CREATE DATABASE rhedcloud_awsaccount_$Elc WITH OWNER = rhedcloud_$Elc ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C' TABLESPACE = pg_default;
COMMENT ON DATABASE rhedcloud_awsaccount_$Elc IS 'RHEDcloud AWS Account Service database for $E';

CREATE DATABASE rhedcloud_ciscoasr_$Elc WITH OWNER = rhedcloud_$Elc ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C' TABLESPACE = pg_default;
COMMENT ON DATABASE rhedcloud_ciscoasr_$Elc IS 'RHEDcloud Cisco ASR Service database for $E';

CREATE DATABASE rhedcloud_elasticip_$Elc WITH OWNER = rhedcloud_$Elc ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C' TABLESPACE = pg_default;
COMMENT ON DATABASE rhedcloud_elasticip_$Elc IS 'RHEDcloud Elastic IP Service database for $E';

CREATE DATABASE rhedcloud_networkops_$Elc WITH OWNER = rhedcloud_$Elc ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C' TABLESPACE = pg_default;
COMMENT ON DATABASE rhedcloud_networkops_$Elc IS 'RHEDcloud Network Ops Service database for $E';

CREATE DATABASE rhedcloud_srd_$Elc WITH OWNER = rhedcloud_$Elc ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C' TABLESPACE = pg_default;
COMMENT ON DATABASE rhedcloud_srd_$Elc IS 'RHEDcloud Security Risk Detection Service database for $E';

CREATE DATABASE rhedcloud_tki_$Elc WITH OWNER = rhedcloud_$Elc ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C' TABLESPACE = pg_default;
COMMENT ON DATABASE rhedcloud_tki_$Elc IS 'RHEDcloud TKI Service database for $E';
EOF

    # use the password of the rhedcloud_ENV user
    export PGPASSWORD=$P

    # AWS Account Service - this is not needed since they're in hibernate.cfg.xml
    #     psql -h 127.0.0.1 rhedcloud_awsaccount_$Elc rhedcloud_$Elc <<EOF
    # CREATE TABLE t_lock (
    #     lock_name varchar(255) NOT NULL,
    #     key_value varchar(255),
    #     create_user varchar(255),
    #     create_date timestamp without time zone,
    #     CONSTRAINT t_lock_pkey PRIMARY KEY (lock_name)
    # );
    # CREATE TABLE t_sequence (
    #     sequence_name varchar(255) NOT NULL,
    #     sequence_value varchar(255),
    #     create_user varchar(255),
    #     create_date timestamp without time zone,
    #     mod_user varchar(255),
    #     mod_date timestamp without time zone,
    #     CONSTRAINT t_sequence_pkey PRIMARY KEY (sequence_name)
    # );
    # CREATE TABLE t_processed_message (
    #     application_id varchar(255) NOT NULL,
    #     processed_message_id varchar(255) NOT NULL,
    #     message_series varchar(255),
    #     create_user varchar(255),
    #     create_date date,
    #     CONSTRAINT t_processed_message_pkey PRIMARY KEY (application_id, processed_message_id)
    # );
    # EOF
    # Cisco ASR Service - just OpenEAI locking tables
    psql -h 127.0.0.1 rhedcloud_ciscoasr_$Elc rhedcloud_$Elc <<EOF
CREATE TABLE t_lock (
    lock_name varchar(255) NOT NULL,
    key_value varchar(255),
    create_user varchar(255),
    create_date timestamp without time zone,
    CONSTRAINT t_lock_pkey PRIMARY KEY (lock_name)
);
EOF
    # Network Ops Service - T_LOCK and T_SEQUENCE needed
    psql -h 127.0.0.1 rhedcloud_networkops_$Elc rhedcloud_$Elc <<EOF
CREATE TABLE t_lock (
    lock_name varchar(255) NOT NULL,
    key_value varchar(255),
    create_user varchar(255),
    create_date timestamp without time zone,
    CONSTRAINT t_lock_pkey PRIMARY KEY (lock_name)
);
CREATE TABLE t_sequence (
   sequence_name varchar(255) NOT NULL,
   sequence_value varchar(255),
   create_user varchar(255),
   create_date timestamp without time zone,
   mod_user varchar(255),
   mod_date timestamp without time zone,
   CONSTRAINT t_sequence_pkey PRIMARY KEY (sequence_name)
);
EOF
    # SRD Service - for now, SRD needs this created since it doesn't have hibernate.cfg.xml
    psql -h 127.0.0.1 rhedcloud_srd_$Elc rhedcloud_$Elc <<EOF
CREATE TABLE t_lock (
    lock_name varchar(255) NOT NULL,
    key_value varchar(255),
    create_user varchar(255),
    create_date timestamp without time zone,
    CONSTRAINT t_lock_pkey PRIMARY KEY (lock_name)
);
EOF
    # TKI Service - nothing needed for TKI

    # it would be nice to have an example for mysql, but
    # for oracle it might look like this instead:
    # CREATE USER tki_dev IDENTIFIED BY $P PROFILE DEFAULT;
    # GRANT CONNECT TO tki_dev;
    # GRANT RESOURCE TO tki_dev;
    # GRANT UNLIMITED TABLESPACE TO tki_dev;
    #
    # CREATE TABLE for some common tables
    #
    # CREATE TABLE T_LOCK (
    #     LOCK_NAME VARCHAR2(255 CHAR) NOT NULL,
    #     KEY_VALUE VARCHAR2(255 CHAR),
    #     CREATE_USER VARCHAR2(255 CHAR),
    #     CREATE_DATE TIMESTAMP,
    #     CONSTRAINT t_lock_pkey PRIMARY KEY (lock_name)
    # );
    #
    # CREATE TABLE t_sequence (
    #     sequence_name VARCHAR2(255 CHAR) NOT NULL,
    #     sequence_value VARCHAR2(255 CHAR),
    #     create_user VARCHAR2(255 CHAR),
    #     create_date TIMESTAMP,
    #     mod_user VARCHAR2(255 CHAR),
    #     mod_date TIMESTAMP,
    #     CONSTRAINT t_sequence_pkey PRIMARY KEY (sequence_name)
    # );

    ) &>rhedcloud-db--create-$Elc.log

    ###############
    ##### TKI keypair and public private keys
    (
    P=$(openssl rand -base64 20)

    echo $E,$P >> tki-keypair--password-$Elc.csv
    openssl genrsa -aes128        -out tki-keypair-$E.pem -passout "pass:$P" 2048
    openssl rsa -pubout            -in tki-keypair-$E.pem -inform PEM -passin "pass:$P" -out tki-keypair--public-key-$E.der  -outform DER
    openssl pkcs8 -topk8 -nocrypt  -in tki-keypair-$E.pem -inform PEM -passin "pass:$P" -out tki-keypair--private-key-$E.der -outform DER

    # keypair pem is not used but is saved as a secret just in case
    aws secretsmanager create-secret --name tki-keypair-$E.pem \
        --secret-string file://tki-keypair-$E.pem \
        --description "TKI keypair for $E" > tki-keypair--pem-secret-$E.json

    # the name of the secret for the public and private keys is important
    # they are known to TKI CLI Client and the TKI Service
    aws secretsmanager create-secret --name tki-public-key-$E.der \
        --secret-binary fileb://tki-keypair--public-key-$E.der \
        --description "TKI public key for $E" > tki-keypair--public-key-secret-$E.json

    aws secretsmanager create-secret --name tki-private-key-$E.der \
        --secret-binary fileb://tki-keypair--private-key-$E.der \
        --description "TKI private key for $E" > tki-keypair--private-key-secret-$E.json

    ) &>tki-keypair--create-$Elc.log

    ###############
    ##### tki-client

    ARN=$(grep \"ARN\" tki-keypair--public-key-secret-$E.json | sed -e 's/^.*"arn:/arn:/' -e 's/".*$//')
    cat <<EOF > tki-client--policy-$E.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "TkiClientSecretsManagerAccess",
            "Effect": "Allow",
            "Action": "secretsmanager:GetSecretValue",
            "Resource": "$ARN"
        },
        {
            "Sid": "TkiClientInstallerDownloads",
            "Effect": "Allow",
            "Action": "s3:GetObject",
            "Resource": [
                "arn:aws:s3:::rhedcloud-aws-$Elc-tki-client/VERSION*.txt",
                "arn:aws:s3:::rhedcloud-aws-$Elc-tki-client/linux-$Elc-installer.tar.gz",
                "arn:aws:s3:::rhedcloud-aws-$Elc-tki-client/mac-$Elc-installer.tar.gz",
                "arn:aws:s3:::rhedcloud-aws-$Elc-tki-client/windows-$Elc-installer.zip"
            ]
        }
    ]
}
EOF
    (
    aws iam create-user --user-name tki-client-$Elc

    aws iam create-policy \
      --policy-name RHEDcloudTkiClient${Ecc}Policy \
      --policy-document file://tki-client--policy-$E.json \
      --description "This policy provides read-only access to the TKI public key for $E in Secrets Manager and Installer downloads in S3."
    aws iam attach-user-policy --user-name tki-client-$Elc \
      --policy-arn "arn:aws:iam::$AWS_ADMIN_ACT:policy/RHEDcloudTkiClient${Ecc}Policy"

    aws iam create-access-key --user-name tki-client-$Elc > tki-client--accesskey-$Elc

    ) &>tki-client--user-$Elc.log



    ###############
    ##### tki-service

    ARN=$(grep \"ARN\" tki-keypair--private-key-secret-$E.json | sed -e 's/^.*"arn:/arn:/' -e 's/".*$//')
    cat <<EOF > tki-service--policy-$E.json
{
    "Version": "2012-10-17",
    "Statement": {
        "Sid": "TkiServiceSecretsManagerAccess",
        "Effect": "Allow",
        "Action": "secretsmanager:GetSecretValue",
        "Resource": "$ARN"
    }
}
EOF
    (
    aws iam create-user --user-name tki-service-$Elc

    aws iam create-policy \
      --policy-name RHEDcloudTkiService${Ecc}Policy \
      --policy-document file://tki-service--policy-$E.json \
      --description "This policy provides read-only access to the TKI private key for $E in Secrets Manager."
    aws iam attach-user-policy --user-name tki-service-$Elc \
      --policy-arn "arn:aws:iam::$AWS_ADMIN_ACT:policy/RHEDcloudTkiService${Ecc}Policy"

    aws iam create-access-key --user-name tki-service-$Elc > tki-service--accesskey-$Elc

    ) &>tki-service--user-$Elc.log

done

###############
##### create secrets for passwords

cat rhedcloud-db--role-password-*.csv > rhedcloud-db--role-passwords.csv
aws secretsmanager create-secret --name rhedcloud-db--role-passwords.csv \
    --secret-string file://rhedcloud-db--role-passwords.csv \
    --description "RHEDcloud DB passwords" > rhedcloud-db--role-passwords-secret.json

cat tki-keypair--password-*.csv > tki-keypair--passwords.csv
aws secretsmanager create-secret --name tki-keypair--passwords.csv \
    --secret-string file://tki-keypair--passwords.csv \
    --description "TKI keypair passwords" > tki-keypair--passwords-secret.json


###############
##### security risk detection service - not per environment

cat <<EOF > security-risk-detection-service--policy.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "dynamodb:*",
            "Resource": "arn:aws:dynamodb:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:table/SecurityRiskDetector*"
        }
    ]
}
EOF
(
aws iam create-user --user-name security-risk-detection-service

aws iam create-policy \
  --policy-name RHEDcloudSecurityRiskDetectionDynamoDBAccess \
  --policy-document file://security-risk-detection-service--policy.json \
  --description "This policy provides full access to SecurityRiskDetector tables."
aws iam attach-user-policy --user-name security-risk-detection-service \
  --policy-arn "arn:aws:iam::$AWS_ADMIN_ACT:policy/RHEDcloudSecurityRiskDetectionDynamoDBAccess"

aws iam create-access-key --user-name security-risk-detection-service > security-risk-detection-service--accesskey

) &>security-risk-detection-service--user.log


###############
##### ciscoasr service - not per environment
##### who ever creates the secrets will have to update the policy

cat <<EOF > ciscoasr-service--policy.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "secretsmanager:GetSecretValue",
            "Resource": [
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-DEV-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R1-NAT-PROD-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R2-NAT-PROD-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R1-VPN-PROD-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R2-VPN-PROD-XXXXX"
            ]
        }
    ]
}
EOF
(
aws iam create-user --user-name ciscoasr-service

aws iam create-policy \
  --policy-name CiscoAsrServiceLabRouterAccess \
  --policy-document file://ciscoasr-service--policy.json \
  --description "Policy to allow access to Cisco ASR Lab Router Credentials."
aws iam attach-user-policy --user-name ciscoasr-service \
  --policy-arn "arn:aws:iam::$AWS_ADMIN_ACT:policy/CiscoAsrServiceLabRouterAccess"

aws iam create-access-key --user-name ciscoasr-service > ciscoasr-service--accesskey

) &>ciscoasr-service--user.log

###############
##### RHEDcloudAdministratorRole

cat <<EOF > RHEDcloudAdministratorRole--policy-admin.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$AWS_ADMIN_ACT:saml-provider/Site_Prod_IDP"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "saml:aud": "https://signin.aws.amazon.com/saml",
          "saml:iss": "https://idp.site.org/idp/shibboleth"
        }
      }
    }
  ]
}
EOF
(
aws iam create-role --path '/rhedcloud/' \
  --role-name RHEDcloudAdministratorRole \
  --description 'RHEDcloud Administrator Role for Customer Administrators' \
  --assume-role-policy-document file://RHEDcloudAdministratorRole--policy-admin.json \
  --max-session-duration 3600
aws iam attach-role-policy --role-name RHEDcloudAdministratorRole \
  --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
) &>RHEDcloudAdministratorRole--create-admin.log

###############
##### RHEDcloudCentralAdministratorRole

cat <<EOF > RHEDcloudCentralAdministratorRole--policy-admin.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$AWS_ADMIN_ACT:saml-provider/Site_Prod_IDP"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "saml:aud": "https://signin.aws.amazon.com/saml",
          "saml:iss": "https://idp.site.org/idp/shibboleth"
        }
      }
    }
  ]
}
EOF
(
aws iam create-role --path '/rhedcloud/' \
  --role-name RHEDcloudCentralAdministratorRole \
  --description 'RHEDcloud Central Administrator Role for Central IT Admins' \
  --assume-role-policy-document file://RHEDcloudCentralAdministratorRole--policy-admin.json \
  --max-session-duration 3600
aws iam attach-role-policy --role-name RHEDcloudCentralAdministratorRole \
  --policy-arn "arn:aws:iam::aws:policy/AdministratorAccess"
) &>RHEDcloudCentralAdministratorRole--create-admin.log

###############
##### RHEDcloudAuditorRole

cat <<EOF > RHEDcloudAuditorRole--policy-admin.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::$AWS_ADMIN_ACT:saml-provider/Site_Prod_IDP"
      },
      "Action": "sts:AssumeRoleWithSAML",
      "Condition": {
        "StringEquals": {
          "saml:aud": "https://signin.aws.amazon.com/saml",
          "saml:iss": "https://idp.site.org/idp/shibboleth"
        }
      }
    }
  ]
}
EOF
(
aws iam create-role --path '/rhedcloud/' \
  --role-name RHEDcloudAuditorRole \
  --description 'RHEDcloud Role for Auditors' \
  --assume-role-policy-document file://RHEDcloudAuditorRole--policy-admin.json \
  --max-session-duration 3600
aws iam attach-role-policy --role-name RHEDcloudAuditorRole \
  --policy-arn "arn:aws:iam::aws:policy/job-function/ViewOnlyAccess"
) &>RHEDcloudAuditorRole--create-admin.log
