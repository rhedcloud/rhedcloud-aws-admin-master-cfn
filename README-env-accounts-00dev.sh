#!/bin/bash

# DEV
# aws organizations list-roots
#   Root                               "arn:aws:organizations::MASTER_ACT:root/o-a1b2c3d4e5/r-a1b2"
# aws organizations list-organizational-units-for-parent --parent-id r-a1b2
#   RHEDcloudStandardAccountOrg        "arn:aws:organizations::MASTER_ACT:ou/o-a1b2c3d4e5/ou-a1b2-a1b2c3d4"
#   RHEDcloudHipaaAccountOrg           "arn:aws:organizations::MASTER_ACT:ou/o-a1b2c3d4e5/ou-a1b2-a1b2c3d5"
#   RHEDcloudAccountAdministrationOrg  "arn:aws:organizations::MASTER_ACT:ou/o-a1b2c3d4e5/ou-a1b2-a1b2c3d6"

export AWS_ACCESS_KEY_ID=dev_aws_access_key_id_goes_here
export AWS_SECRET_ACCESS_KEY=dev_aws_secret_access_key_goes_here
export AWS_DEFAULT_REGION=us-east-1

export MASTER_ACT=dev_aws_account_id_goes_here
export MASTER_ORG_ROOT=r-a1b2
export MASTER_ORG_RHEDcloudStandardAccountOrg=ou-a1b2-a1b2c3d4
export MASTER_ORG_RHEDcloudHipaaAccountOrg=ou-a1b2-a1b2c3d5
export MASTER_ORG_RHEDcloudAccountAdministrationOrg=ou-a1b2-a1b2c3d6


###### Site Dev 1 (Standard)

export ACCOUNT_ID=unknown
export ACCOUNT_EMAIL='aws-dev-1@site.org'
export ACCOUNT_NAME='Site Dev 1'
export ACCOUNT_ALIAS=site-dev-1
export ACCOUNT_AddHIPAAIAMPolicy=No
export MASTER_ORG_RHEDcloudAccountDestination=$MASTER_ORG_RHEDcloudStandardAccountOrg

aws organizations create-account --email "$ACCOUNT_EMAIL" --account-name "$ACCOUNT_NAME" --iam-user-access-to-billing ALLOW \
        &>baseaccount-$MASTER_ACT-$ACCOUNT_ALIAS--create.log
cat baseaccount-$MASTER_ACT-$ACCOUNT_ALIAS--create.log
aws organizations describe-create-account-status --create-account-request-id car-xxx

aws organizations describe-create-account-status --create-account-request-id car-xxx | jq -r '.CreateAccountStatus.AccountId'
export ACCOUNT_ID=xxx

source README-env-accounts-00a.sh

###### Site Dev 2 (Standard)

export ACCOUNT_EMAIL='aws-dev-2@site.org'
export ACCOUNT_NAME='Site Dev 2'
export ACCOUNT_ALIAS=site-dev-2
export ACCOUNT_AddHIPAAIAMPolicy=No
export MASTER_ORG_RHEDcloudAccountDestination=$MASTER_ORG_RHEDcloudStandardAccountOrg

aws organizations create-account --email "$ACCOUNT_EMAIL" --account-name "$ACCOUNT_NAME" --iam-user-access-to-billing ALLOW \
        &>baseaccount-$MASTER_ACT-$ACCOUNT_ALIAS--create.log
cat baseaccount-$MASTER_ACT-$ACCOUNT_ALIAS--create.log
aws organizations describe-create-account-status --create-account-request-id car-xxx

aws organizations describe-create-account-status --create-account-request-id car-xxx | jq -r '.CreateAccountStatus.AccountId'
export ACCOUNT_ID=xxx

source README-env-accounts-00a.sh

###### Site Dev 3 (HIPAA)

export ACCOUNT_EMAIL='aws-dev-3@site.org'
export ACCOUNT_NAME='Site Dev 3'
export ACCOUNT_ALIAS=site-dev-3
export ACCOUNT_AddHIPAAIAMPolicy=Yes
export MASTER_ORG_RHEDcloudAccountDestination=$MASTER_ORG_RHEDcloudHipaaAccountOrg

aws organizations create-account --email "$ACCOUNT_EMAIL" --account-name "$ACCOUNT_NAME" --iam-user-access-to-billing ALLOW \
        &>baseaccount-$MASTER_ACT-$ACCOUNT_ALIAS--create.log
cat baseaccount-$MASTER_ACT-$ACCOUNT_ALIAS--create.log
aws organizations describe-create-account-status --create-account-request-id car-xxx

aws organizations describe-create-account-status --create-account-request-id car-xxx | jq -r '.CreateAccountStatus.AccountId'
export ACCOUNT_ID=xxx

source README-env-accounts-00a.sh
