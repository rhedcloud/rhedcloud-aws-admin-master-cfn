#!/bin/bash

# See README-env-accounts-00.md for instructions
# Env setup:
#    sudo apt-get install python3-pip
#    sudo apt install python3-pytest
#    pip3 install -r ../../rhedcloud-aws-pipeline-scripts/requirements.txt

export PATH=../../rhedcloud-aws-pipeline-scripts:$PATH
export PYTHONPATH=../../rhedcloud-aws-python-testutils

# RHEDCLOUD_ASSUME_ROLE is needed by cfn_tool because the RHEDcloudMaintenanceOperatorRole doesn't exist yet in the account
export RHEDCLOUD_ASSUME_ROLE=role/OrganizationAccountAccessRole

# common

#aws organizations move-account --account-id ${ACCOUNT_ID} --source-parent-id ${MASTER_ORG_ROOT} --destination-parent-id ${MASTER_ORG_RHEDcloudAccountAdministrationOrg}

# AWS Console - switch role using OrganizationAccountAccessRole
# aws sts assume-role --role-session-name ${ACCOUNT_ALIAS} --role-arn arn:aws:iam::${ACCOUNT_ID}:role/OrganizationAccountAccessRole

# https://bitbucket.org/rhedcloud/rhedcloud-aws-rs-account-cfn/src/master/rhedcloud-aws-rs-account-cfn.json

# To fix this - pipelines must be setup and executed to create cfn templates and upload them to Master account S3 buckets.

cat <<EOF > baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--spec-rs-account.json
{
  "account": "${ACCOUNT_ID}",
  "parameters": {
    "CloudTrailName": "${ACCOUNT_ALIAS}-ct1",
    "AddHIPAAIAMPolicy": "${ACCOUNT_AddHIPAAIAMPolicy}",
    "RHEDcloudIDP": "RHEDcloud_Prod_IDP",
    "RHEDcloudSamlIssuer": "https://idp.rhedcloud.org/idp/shibboleth",
    "RHEDcloudSecurityRiskDetectionServiceUserArn": "arn:aws:iam::${MASTER_ACT}:user/RHEDcloudSecurityRiskDetectionService",
    "RHEDcloudAwsAccountServiceUserArn": "arn:aws:iam::${MASTER_ACT}:user/RHEDcloudAwsAccountService",
    "RHEDcloudMaintenanceOperatorRoleArn": "arn:aws:iam::${MASTER_ACT}:role/rhedcloud/RHEDcloudMaintenanceOperatorRole"
  },
  "stack_name": "rhedcloud-aws-rs-account",
  "template": "s3://local-rhedcloud-aws-${ENV}-cfn-templates/rhedcloud-aws-rs-account-cfn.compact.json",
  "region": "${AWS_DEFAULT_REGION}"
}
EOF

cfn_tool.py apply baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--spec-rs-account.json
cfn_tool.py verify rhedcloud-aws-rs-account
cfn_tool.py verify rhedcloud-aws-rs-account -r ${AWS_DEFAULT_REGION} --ou RHEDcloudAccountAdministrationOrg

# CIDRs below are for member "admin" account
#
cat <<EOF > baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--spec-vpc-type1.json
{
  "account": "${ACCOUNT_ID}",
  "parameters": {
    "VpcCidr": "10.64.60.0/23",
    "ManagementSubnet1Cidr": "10.64.60.0/26",
    "ManagementSubnet2Cidr": "10.64.60.64/26",
    "PublicSubnet1Cidr": "10.64.60.128/26",
    "PublicSubnet2Cidr": "10.64.60.192/26",
    "PrivateSubnet1Cidr": "10.64.61.0/25",
    "PrivateSubnet2Cidr": "10.64.61.128/25",
    "ConnectVpn": "No",
    "DeployInferenceEc2Endpoint": "No"
  },
  "stack_name": "rhedcloud-aws-vpc-type1",
  "template": "s3://local-rhedcloud-aws-${ENV}-cfn-templates/rhedcloud-aws-vpc-type1-cfn.compact.json",
  "region": "${AWS_DEFAULT_REGION}"
}
EOF

cfn_tool.py apply baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--spec-vpc-type1.json
cfn_tool.py verify rhedcloud-aws-vpc-type1
cfn_tool.py verify rhedcloud-aws-vpc-type1 -r ${AWS_DEFAULT_REGION} --ou RHEDcloudAccountAdministrationOrg

aws sts assume-role --role-session-name base-account-provisioning \
  --role-arn arn:aws:iam::${ACCOUNT_ID}:role/OrganizationAccountAccessRole &>baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--assume-role-session.json
(
export AWS_ACCESS_KEY_ID=$(cat baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--assume-role-session.json | jq -r '.Credentials.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(cat baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--assume-role-session.json | jq -r '.Credentials.SecretAccessKey')
export AWS_SESSION_TOKEN=$(cat baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--assume-role-session.json | jq -r '.Credentials.SessionToken')

# TODO: SAML IDP and SP required
# aws iam create-saml-provider --name RHEDcloud_Prod_IDP --saml-metadata-document file://Rhedcloud_Prod_SAML_metadata_document.xml
#aws iam create-account-alias --account-alias ${ACCOUNT_ALIAS}
)

if [[ "${MASTER_ORG_RHEDcloudAccountDestination}" == "${MASTER_ORG_RHEDcloudAccountAdministrationOrg}" ]]; then
    echo "Destination account is admin OU - skipping move account."
else
    aws organizations move-account --account-id ${ACCOUNT_ID} \
      --source-parent-id ${MASTER_ORG_RHEDcloudAccountAdministrationOrg} \
      --destination-parent-id ${MASTER_ORG_RHEDcloudAccountDestination}
fi


