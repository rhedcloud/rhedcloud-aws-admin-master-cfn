# RHEDcloud Implementation Guide

## CloudFormation for deploying RHEDcloud admin account

### Requirements before starting
- Create AWS Admin account and four AWS Master accounts
- AWS Certificate (ACM) for load balancers, wild card for domain to be used ie *.rhedcloud.site.org - [ACM](https://console.aws.amazon.com/acm/home?region=us-east-1#/privatewizard/)
- In the Admin account, request an increase in the limits for Classic Load Balancers to 60 - [Overview](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-limits.html) or [Specific Request URL](https://console.aws.amazon.com/servicequotas/home?region=us-east-1#!/services/elasticloadbalancing/quotas)
- In the Admin account, increase EC2 [limits](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-resource-limits.html).
  Specifically, increase `Running On-Demand All Standard (A, C, D, H, I, M, R, T, Z) instances` to 2000 - [specific url](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Limits:)
- Gather CIDR block information for VPC from the implementing site's networking team - requires a RFC1918 IPv4 private /23 - [AWS VPC and subnet sizing for IPv4](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html#vpc-sizing-ipv4) - see defaults for subnet splits
- In the AWS Admin account, create a bucket to store CFN templates which is
  used in the master script to define nested template location i.e. SITE-rhedcloud-aws-prod-cfn-templates
- The Elastic Beanstalk instances require an EC2 key pair.
  Create one in the Admin account or import an existing key pair. Note: This key is highly sensitive and should be stored securely according to your site’s security requirements - [console url](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#KeyPairs:) or [cli url](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ec2/create-key-pair.html)

### Notes
base-00-master-rhedcloud-aws-cfn.yml is a master template that will deploy
- IAM roles for Beanstalk and the artifact deploy service accounts
- CloudFormation Macros for string transforms
- S3 buckets for build artifacts and application configs
- VPC/Subnets/Routing
- Beanstalk Application bases

env-00-master-rhedcloud-aws-environment-cfn.yml is a master template that replies on values exported from the base master template.
This template is used to setup the environments, ie dev/stage/test/production.
It creates the following with environment specific naming/tags.
- DynamoDB tables with autoscaling
- Amazon MQ
- S3 bucket for DocUri
- RDS (PostgreSQL, MySQL or Oracle)
- Beanstalk stacks, required and optional ones enabled during master template deploy.

## Post Template Configuration
After the base-00-master and env-00-master templates have run there is a bit more setup needed in the AWS Admin and Master accounts.

1. In the admin account, manually delete all default VPCs in all regions
1. Follow the instructions in [admin account configuration](README-base-00.sh)
1. Follow the instructions in [environment accounts configuration](README-env-00.sh)
    1. Both of these do essential setup in the various AWS Accounts, like
        1. Create AWS Organizations and OUs
        1. Create AWS IAM resources for RHEDcloud service accounts
        1. Create AWS RDS users and structures
        1. Create AWS S3 buckets for CFN Templates
        1. Create RHEDcloud TKI Client / Service keys and secrets and installer buckets
1. The creation of the AWS Organizations in the last step must be verified by the root user on the AWS Account.
1. Run pipeline for site-aws-org-standard-scp
1. Run pipeline for site-aws-org-hipaa-scp
1. Attach SCPs to OUs by following the instructions in [attach SCPs](README-env-01.sh)
1. Run pipeline for site-aws-rs-account-cfn
1. Run pipeline for site-aws-vpc-type1-cfn
1. See [Base account creation](#markdown-header-base-account-creation)
1. TODO - none of the beanstalk environments have notification email address
1. TODO - beanstalk environments have load balancer visibility as public - should be internal
1. See [RHEDcloud Monitoring Setup Guide](https://rhedcloud.atlassian.net/wiki/spaces/RP/pages/595460097/RHEDcloud+Monitoring+Setup+Guide)


#### Account Access for Implementation Team Members
As part of creating the Admin and Master accounts, users should be created for members of the implementation team
and they will be given AWS `AdministratorAccess`.

#### Bitbucket and App Password and BB_AUTH_STRING
The Site will procure a Bitbucket account (possibly on the Academic plan).
The Free plan does not have enough build minutes to support RHEDcloud pipelines.
A new team should be created and members of the implementation team can be added with Admin permissions.

The deploy-only pipelines will need read/write access to the Bitbucket repositories.
The contact at Site will create a service user with an app password granting
Repository-Read and Repository-Write permissions.  The service user name and app password
will make up the BB_AUTH_STRING pipeline variable.

#### CNAME for Services
The Elastic Beanstalk instances running our services get a URL.
We add CNAME records to have a simpler naming inside the sites domain that was issued in
the certificate used in the Elastic Beanstalk load balancers.

For example, the environment URL can look
like [this](https://rhedcloud-awsaccount-service-dev.a1b2c3d4e5.us-east-1.elasticbeanstalk.com/),
but that URL can change if the environment is terminated.  Instead, we want a
consistent name like [this](https://rhedcloud-awsaccount-service-dev.rhedcloud.site.org/)

1. Create CNAME records for all services in Elastic Beanstalk
    1. `aws --profile me_in_site_admin elasticbeanstalk describe-environments | jq -r '.Environments[].CNAME' | sort`

#### Application Config Buckets
RHEDcloudBeanstalkAppsConfigBucket in base-01-rhedcloud-aws-s3-cfn.yml creates a S3 bucket for each
environment to store the configuration documents for the RHEDcloud services.  For example, RHEDcloud has buckets
named `rhedcloud-config-dev`, `rhedcloud-config-test`, etc.

The config docs contain secrets so access to the buckets is tightly controlled using several condition keys
like [userid](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_condition-keys.html#condition-keys-userid)
and [SourceVpce](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_condition-keys.html#condition-keys-sourcevpce).

After the CFN templates have run, the permissions and policy on these buckets must be adjusted to allow
implementation team members to upload content.  The first step is to adjust the policy to something similar to:
```json
    {
        "Version": "2008-10-17",
        "Statement": [
            {
                "Sid": "VPCe and UserIds",
                "Effect": "Deny",
                "Principal": "*",
                "Action": "s3:*",
                "Resource": [
                    "arn:aws:s3:::BUCKET_NAME",
                    "arn:aws:s3:::BUCKET_NAME/*"
                ],
                "Condition": {
                    "StringNotLike": {
                        "aws:sourceVpce": "vpce-a1b2c3d4",
                        "aws:userid": [
                            "AIDAabc",
                            "AROAdef",
                            "ADMIN_ACCOUNT_ID"
                        ],
                        "aws:username": [
                            "me_in_site_admin@example.com",
                            "you_in_site_admin@example.com"
                        ]
                    }
                }
            }
        ]
    }
```

Where the [userid and username](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_identifiers.html#identifiers-unique-ids)
can be determined by running `aws --profile me_in_site_admin iam list-users` and the SourceVpce can be
determined by running `aws --profile me_in_site_admin ec2 describe-vpc-endpoints`.

The second step is to turn OFF `Block public access`.

Third step is to enable Public access ACL for 'List objects'.

These last two steps may seem to contradict the idea that the bucket contents remains secure but the
policy is very restrictive and achieves that goal.

The final step is to copy config docs into the bucket.  The RHEDcloud config docs can be used as a seed.
1. Copy RHEDcloud config docs to your
 laptop: `aws --profile me_in_rhedcloud_admin s3 sync s3://rhedcloud-config-dev $HOME/s3-config-rhedcloud-dev`
1. Copy to Site: `mkdir $HOME/s3-config-site-dev ; cp -R $HOME/s3-config-rhedcloud-dev $HOME/s3-config-site-dev`
1. Remove files: `rm $HOME/s3-config-site-dev/configs/messaging/Environments/Examples/Deployments/*`
1. Push: `aws --profile me_in_site_admin s3 sync $HOME/s3-config-site-dev s3://site-rhedcloud-config-dev --acl public-read`


#### Email Distribution List Provisioning
An email address is automatically assigned by AWS Account Service when a new account is provisioned.
The email address is a distribution list that contains the Account Owner, Requestor, and Administrators.

Some sites have email services that accept requests to create new distribution list on-demand.
If no such system is available, then a bunch of lists have to be pre-provisioned in advance.

The email addresses follow a pattern based on the environment and index of the new account.
For example, aws-dev-[1234...]@site.org or aws-test-[1234...]@site.org, etc.
For production, the pattern is aws-account-[1234...]@site.org as that is more friendly naming for users.

TODO - When an account is provisioned in AWS Account Service, users are added to certain roles which causes
the IDM to sync users into the distribution lists.

#### Base account creation
It may be a while until new accounts can be provisioned by AWS Account Service.
So that users and developers have early access to AWS accounts in an environment,
we manually create three sample accounts.
They are always the first few accounts in an environment series.

Note, email distribution lists should exist before creating base accounts.

1. Follow the instructions in [base aws accounts](README-env-accounts-00.md)
1. Follow the instructions in [account metadata](README-env-accounts-01.sh)


#### Site to Site VPN
`From some correspondence about VPNs`
It is my understanding that there are two different tunnels needed. The
tunnel through the ASA is only to allow access to our IAM resources for
authenticating users and you've already provided a configuration so now
that we have upgraded to 9.7+ we should be able to activate that site to
site VPN.

The "automated VPN and NAT provisioning" you mentioned is planned through
our ASR1K which we already have configured. This is the one you assisted
me with awhile back.

#### VPN Access
During development, it is important to be able to login directly to ec2 instances and gain access to the databases.

1. Get a sponsored account with Site (like NetID access) that includes VPN access to their network.

## Service Configurations
1. TODO - All services are secured by way of rhedcloud-ebextensions
1. AWS Account Service will send email notifications to users.
    1. TODO - need email server connection info
    1. TODO - goes in config doc in ConsumerConfig(AccountNotificationSyncCommand,UserNotificationSyncCommand)/MailServiceConfigs/MailHost
    1. Make sure the RS and type1 CFN templates have been promoted into the S3 buckets
    1. The AwsServiceDetectionApplication component uses the AWS Support API which requires at
       least a Business support plan.  It can be commented out in the app config to disable.
       It just means you won't have any AWS service information loaded into the service registry.
       There have been discussions about, one day, being able to copy the service inventory from RHEDcloud into Site.
    1. TODO - [Service Quotas](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/quotas.html)
              for SES must be raised because many emails can be sent as part of normal operations
        1. See Notes 1 and 2 below
1. TODO - Security Risk Assessments should be imported into AWS Account Service metadata
1. TODO - AWS Service Inventory ?? - need importing?
1. TODO - TKI Service needs Duo integration keys
1. TODO - TKI Service needs a login URL that will release SAML Assertions
1. TODO - TKI Service must be able to receive requests without the sender being connected to VPN of the Site
    1. Likely means adding Site Firewall exceptions
1. Email Address Validation Service uses a third-party provider to validate email address
    1. Currently, we use NeverBounce so an account must be procured
1. TODO - RHEDcloud Landing and Console need ...
    1. Make sure Elastic Beanstalk uses deprecated httpd 2.2
    1. During the deploy-only pipeline, site logo replaces the images/emory-logo-154x45.png
    1. RHEDcloud Console DEV https://dev.cloudconsole.site.org/
    1. RHEDcloud Console PROD https://cloudconsole.app.site.org/

```
Note 1 - example text for limit increase request
 We have applications running in a private, isolated VPC connected to our on-prem
 network via site-to-site VPN that need to send e-mail to the Site's users via their
 SMTP server <host URL>. We have previously run these applications in
 a VPC in other AWS accounts, and we applied to have the SMTP throttling
 removed successfully. Now we are implementing these applications in the VPC in this
 new account. These applications run in Elastic Beanstalk and can come up on
 any IP number in our two private subnets, so we need general restrictions lifted
 and not for a specific ec2 instance or elastic IP. Again, this is in a private VPC
 with no access to the internet other than through our Rice University on-prem network.
```

```
Note 2
Because reverse DNS record entries are commonly considered in anti-spam filters, we recommend assigning a reverse DNS record to the Elastic IP address you use to send email to third parties. Please use the form located at this link to request a reverse DNS entry:
https://aws-portal.amazon.com/gp/aws/html-forms-controller/contactus/ec2-email-limit-rdns-request

If you'd like to proceed with assigning a reverse DNS record to the Elastic IP, the first step would be to configure the A record for the domain to match the desired PTR record on your side.

Please follow the instructions at the link below to create the A record:
https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-creating.html
```
