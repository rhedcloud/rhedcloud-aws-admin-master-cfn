#Admin Account Migration

## Status
 
 ```rhedcloud-admin-1``` to ```local-rhedcloud-prod-0```

1. The target account (```local-rhedcloud-prod-0```) has been created using modified script ```README-env-accounts-00prod-local.sh```

2. Base templates have been applied to target account

## Next Steps

The environment templates - top level template ```env-00-master-rhedcloud-aws-environment-cfn.yml```, requires certificates on the domain. Because we want the source and destination accounts to remain operational during the migration, there are some domain hosting issues. Below are several options to standup hosted zones on both accounts.

1. Use the same domain and switch nameservers in the Domain Registrar. This allows both accounts to stay viable, in a mutually exclusive manner.
2. Migrate the hosted zone records from the source to the destination account. This has the drawback that the source account's hosted zone would need to be terminated, and the Domain Registrar be updated for the destination account, thus stopping all RHEDcloud provisioning on the source account.
3. Centralize DNS management in a multi account environment using centralized DNS forwarders outside of the accounts.
4. Purchase a new temporary domain, e.g. prod.rhedcloud.org.
