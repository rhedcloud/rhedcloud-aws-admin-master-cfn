#!/bin/bash

echo copy/edit this template script to README-env-01-site.sh
echo chmod u+x README-env-01-site.sh
echo mkdir rhedcloud-site-setup
echo cd rhedcloud-site-setup
echo ../README-env-01-site.sh 2>&1 \| tee -a README-env-01-site.log
exit

command -v python3 >/dev/null 2>&1 || { echo >&2 "Python 3 required but it's not installed.  Aborting."; exit 1; }
set -x

# One time install of python3 and several dependencies.  For example, on my mac
# brew install python3
# brew link python3
# pip3 install botocore boto3 cfn_flip pytest docker jsonschema

### Start of Variables to edit ###

# paths to scp_tool.py and supporting library
export PATH=/directory/containing/rhedcloud-aws-pipeline-scripts:$PATH
export PYTHONPATH=/directory/containing/rhedcloud-aws-python-testutils:$PYTHONPATH

# AWS Region to operate in
export AWS_DEFAULT_REGION=us-east-1

# ENVIRONMENTS is an array of Environments to loop through
ENVIRONMENTS=(
  "DEV"
  "TEST"
  "STAGE"
  "PROD"
)

### End of Variables to edit ###

date

for E in "${ENVIRONMENTS[@]}" ; do
    Elc=$(echo $E | tr '[:upper:]' '[:lower:]')

    case $E in
      DEV)
        export AWS_ACCESS_KEY_ID=dev_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=dev_aws_secret_access_key_goes_here
        ;;
      TEST)
        export AWS_ACCESS_KEY_ID=test_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=test_aws_secret_access_key_goes_here
        ;;
      STAGE)
        export AWS_ACCESS_KEY_ID=stage_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=stage_aws_secret_access_key_goes_here
        ;;
      PROD)
        export AWS_ACCESS_KEY_ID=prod_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=prod_aws_secret_access_key_goes_here
        ;;
    esac


    ###############
    ##### Verify SCPs
    ##### Verify SCPs
    (
    scp_tool.py verify standard
    scp_tool.py verify hipaa
    ) &>scp--verify-$Elc-master.log

    ###############
    ##### Attach SCPs
    # To apply the new version of the SCP,
    #  first, promote latest RHEDcloud SCPs to Sites S3 bucket, by running pipelines site-aws-org-standard-scp and site-aws-org-hipaa-scp
    (
    scp_tool.py apply s3://site-aws-dev-scps/rhedcloud-aws-org-standard-scp-with-metadata.json RHEDcloudStandardAccountOrg
    scp_tool.py apply s3://site-aws-dev-scps/rhedcloud-aws-org-hipaa-scp-with-metadata.json    RHEDcloudHipaaAccountOrg
    ) &>scp--attach-$Elc-master.log

    ###############
    ##### Attach Specific Version of an SCP
    # A specific version of the RHEDcloud SCPs can be promoted to Sites S3 bucket
    #cd rhedcloud-aws-org-hipaa-scp
    #BITBUCKET_BUILD_NUMBER=140+13 scp_tool.py apply rhedcloud-aws-org-hipaa-scp.json RHEDcloudHipaaAccountOrg

done
