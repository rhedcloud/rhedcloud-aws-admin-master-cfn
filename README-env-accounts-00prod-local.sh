#!/bin/bash

# DEV
# aws organizations list-roots
#   Root                               "arn:aws:organizations::MASTER_ACT:root/o-a1b2c3d4e5/r-a1b2"
# aws organizations list-organizational-units-for-parent --parent-id r-a1b2
#   RHEDcloudStandardAccountOrg        "arn:aws:organizations::MASTER_ACT:ou/o-a1b2c3d4e5/ou-a1b2-a1b2c3d4"
#   RHEDcloudHipaaAccountOrg           "arn:aws:organizations::MASTER_ACT:ou/o-a1b2c3d4e5/ou-a1b2-a1b2c3d5"
#   RHEDcloudAccountAdministrationOrg  "arn:aws:organizations::MASTER_ACT:ou/o-a1b2c3d4e5/ou-a1b2-a1b2c3d6"


#########################################################################################
# Wait for create account to complete by:
#
# 1) Parsing CAR ID from create account response, passed in as log file name reference
# 2) Use the CAR ID to poll AWS for account id using describe create account status
#
#########################################################################################
function wait-for-create-account() {

    local CREATE_ACCOUNT_LOG_FILE_NAME=$1

    # Obtain car id from initial results of create-account invocation
    CAR_ID=$(cat ${CREATE_ACCOUNT_LOG_FILE_NAME} | jq -r '.CreateAccountStatus.Id')

    # Wait for it...
    sleep 30

    # Describe create account status with obtained car id
    export ACCOUNT_ID=$(aws organizations describe-create-account-status --create-account-request-id ${CAR_ID} | jq -r '.CreateAccountStatus.AccountId')

    # Check for account id - if not present, retry
    if [[ -z "$ACCOUNT_ID" ]]; then
        echo Created member account id no available yet - sleeping 10 more seconds...
        sleep 10
        ACCOUNT_ID=$(aws organizations describe-create-account-status --create-account-request-id ${CAR_ID} | jq -r '.CreateAccountStatus.AccountId')
    fi

    if [[ -z "${ACCOUNT_ID}" ]]; then
        echo "No account id after two tries!"
    fi

}

# PROD master account credentials/ids/keys
export ENV=prod
export ENV_NAME=Prod

# User: RHEDcloudOrgMasterSetup
export AWS_ACCESS_KEY_ID=Master_access_key
export AWS_SECRET_ACCESS_KEY=Master_secret_key
export AWS_DEFAULT_REGION=us-east-1

# local-rhedcloud-prod-master
export MASTER_ACT=784619214844
export MASTER_ORG_ROOT=r-ls05
export MASTER_ORG_RHEDcloudStandardAccountOrg=ou-ls05-hglchsfa
export MASTER_ORG_RHEDcloudHipaaAccountOrg=ou-ls05-exjni6ka
export MASTER_ORG_RHEDcloudAccountAdministrationOrg=ou-ls05-r38wl67s

###### RHEDcloud Local Prod 0 (Admin/Control)

export ACCOUNT_ID=unknown
export ACCOUNT_EMAIL="aws-${ENV}-0@rhedcloud.org"
export ACCOUNT_NAME="RHEDcloud ${ENV_NAME} 0"
export ACCOUNT_ALIAS=local-rhedcloud-${ENV}-0
export ACCOUNT_AddHIPAAIAMPolicy=No
# Put admin member account into master admin org?
export MASTER_ORG_RHEDcloudAccountDestination=${MASTER_ORG_RHEDcloudAccountAdministrationOrg}

echo ${ACCOUNT_EMAIL}
echo ${ACCOUNT_NAME}
echo ${ACCOUNT_ALIAS}


#CREATE_ACCOUNT_LOG_FILE_NAME="baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--create.log"
#
#aws organizations create-account --email "${ACCOUNT_EMAIL}" --account-name "${ACCOUNT_NAME}" --iam-user-access-to-billing ALLOW \
#        &>"$CREATE_ACCOUNT_LOG_FILE_NAME"
#
#wait-for-create-account ${CREATE_ACCOUNT_LOG_FILE_NAME}
#
#echo ${ACCOUNT_ID}
#
#if [[ -z "$ACCOUNT_ID" ]]; then
#    echo "No account id found for alias ${ACCOUNT_ALIAS}... aborting!"
#    exit
#fi

#local-rhedcloud-prod-0   (Admin account)
export ACCOUNT_ID=271882997152

source ../README-env-accounts-00a-local.sh


###### RHEDcloud Local Test 1 (Standard)

#export ACCOUNT_ID=unknown
#export ACCOUNT_EMAIL="aws-${ENV}-1@rhedcloud.org"
#export ACCOUNT_NAME="RHEDcloud ${ENV_NAME} 1"
#export ACCOUNT_ALIAS=local-rhedcloud-${ENV}-1
#export ACCOUNT_AddHIPAAIAMPolicy=No
#export MASTER_ORG_RHEDcloudAccountDestination=${MASTER_ORG_RHEDcloudStandardAccountOrg}
#
#echo ${ACCOUNT_EMAIL}
#echo ${ACCOUNT_NAME}
#echo ${ACCOUNT_ALIAS}
#
#
#CREATE_ACCOUNT_LOG_FILE_NAME="baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--create.log"
#
#aws organizations create-account --email "${ACCOUNT_EMAIL}" --account-name "${ACCOUNT_NAME}" --iam-user-access-to-billing ALLOW \
#        &>"$CREATE_ACCOUNT_LOG_FILE_NAME"
#
#wait-for-create-account ${CREATE_ACCOUNT_LOG_FILE_NAME}
#
#echo ${ACCOUNT_ID}
#
#if [[ -z "${ACCOUNT_ID}" ]]; then
#    echo "No account id found for alias ${ACCOUNT_ALIAS}... aborting!"
#    exit
#fi
#
#source ../README-env-accounts-00a-local.sh

###### Site Dev 2 (Standard)

#export ACCOUNT_EMAIL="aws-${ENV}-2@rhedcloud.org"
#export ACCOUNT_NAME="RHEDcloud ${ENV_NAME} 2"
#export ACCOUNT_ALIAS=local-rhedcloud-${ENV}-2
#export ACCOUNT_AddHIPAAIAMPolicy=No
#export MASTER_ORG_RHEDcloudAccountDestination=${MASTER_ORG_RHEDcloudStandardAccountOrg}
#
#CREATE_ACCOUNT_LOG_FILE_NAME="baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--create.log"
#
#aws organizations create-account --email "${ACCOUNT_EMAIL}" --account-name "${ACCOUNT_NAME}" --iam-user-access-to-billing ALLOW \
#        &>"$CREATE_ACCOUNT_LOG_FILE_NAME"
#
#export ACCOUNT_ID=$(wait-for-create-account ${CREATE_ACCOUNT_LOG_FILE_NAME})
#
#echo ${ACCOUNT_ID}
#
#if [[ -z "$ACCOUNT_ID" ]]; then
#    echo "No account id found for alias ${ACCOUNT_ALIAS}... aborting!"
#    exit
#fi
#
#source ../README-env-accounts-00a-local.sh

###### Site Dev 3 (HIPAA)

export ACCOUNT_EMAIL="aws-${ENV}-3@rhedcloud.org"
export ACCOUNT_NAME="RHEDcloud ${ENV_NAME} 3"
export ACCOUNT_ALIAS=local-rhedcloud-${ENV}-3
export ACCOUNT_AddHIPAAIAMPolicy=Yes
export MASTER_ORG_RHEDcloudAccountDestination=${MASTER_ORG_RHEDcloudHipaaAccountOrg}

#CREATE_ACCOUNT_LOG_FILE_NAME="baseaccount-${MASTER_ACT}-${ACCOUNT_ALIAS}--create.log"
#
#aws organizations create-account --email "${ACCOUNT_EMAIL}" --account-name "${ACCOUNT_NAME}" --iam-user-access-to-billing ALLOW \
#        &>"$CREATE_ACCOUNT_LOG_FILE_NAME"
#
#export ACCOUNT_ID=$(wait-for-create-account ${CREATE_ACCOUNT_LOG_FILE_NAME})
#
#echo ${ACCOUNT_ID}
#
#if [[ -z "$ACCOUNT_ID" ]]; then
#    echo "No account id found for alias ${ACCOUNT_ALIAS}... aborting!"
#    exit
#fi
#
#source ../README-env-accounts-00a-local.sh
