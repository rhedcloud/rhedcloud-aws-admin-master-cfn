#!/bin/bash

# new hosted zone id: Z0035911TV3ICKO03KY2
function convert_to_create() {

    LIST_FILE_NAME=$1
    OUTPUT_FILE_NAME=$(sed 's/.json/_create.json/g' <<< ${LIST_FILE_NAME})
    echo ${OUTPUT_FILE_NAME}


    # Convert to create format.
    cat ${LIST_FILE_NAME} | jq '.ResourceRecordSets |{"Changes":[.[] |select(.Type!="SOA") |select(.Type!="NS") |{"Action":"CREATE","ResourceRecordSet":.}]}' > ${OUTPUT_FILE_NAME}


}

#convert_to_create "zone-records_rhedcloud-admin-1_rhed.cloud.json"
convert_to_create "zone-records_rhedcloud-admin-1_rhedcloud.org.json"
convert_to_create "zone-records_rhedcloud-admin-1_vpc.rhedcloud.org.json"
convert_to_create "zone-records_local.rhedcloud.org.json"

#cat zone-records_rhedcloud-admin-1_rhed.cloud.json | jq '.ResourceRecordSets |{"Changes":[.[] |select(.Type!="SOA") |select(.Type!="NS") |{"Action":"CREATE","ResourceRecordSet":.}]}'

