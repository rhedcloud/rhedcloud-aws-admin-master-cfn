Hello,

Warm Greetings from AWS Premium Support! I am Syed and I will be assisting you with the case today.

I understand that you have created a hosted zone for domain "rhedcloud.org
" in account-A(src-account) and another hosted zone for the same domain in account-B(dest-account).
Next, you wish to resolve the DNS queries for your resources within Account-B(dest-account) with new hosted zone as well as hosted zone in Account-A(src-account) with the same domain "rhedcloud.org
" and eventually shutdown the source account.
Please correct me if I misunderstood your concern.

Before moving to the answers, it is important for us to understand these two concepts correctly-
1. When you create a new Hosted Zone, AWS Route53 creates/ assigns a set of four domain name servers (“NS records”) to this Hosted Zone. Any DNS query to your domain name, (and) when sent to these Name Servers, will be resolved with appropriate IP addresses as per the DNS Record set in your Hosted Zone.

2. Next, in order for any (internet originating) DNS query to your domain to be directed to the correct set of Name Servers (Authoritative Servers), you should update the Name Server record information in your Domain Registrar (GoDaddy!).

”Does Route53 support multiple hosted zones for a single domain?
-> Yes. However, only the NS server record set updated in your registrar will be resolvable from the internet.

“Does Route53 support multiple hosted zones for a single domain across multiple accounts?"
-> As explained above, this is possible, but only the NS Record Set updated with your registrar will be resolvable from the internet.

With this information, let us try to address your questions-

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1. "I'd like to support the same domain name in Route53 in both accounts, for a limited period of time, then eventually shutdown the source account. "
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

As mentioned above this would not be possible as only one hosted zone will be able to resolve the DNS Queries. The Hosted Zone with same Name Server record information as on your Domain Registrar will be the one used for resolving DNS Queries .

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
2. Request certificates in the destination account that will validate?
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

I would like to inform you that you will be able to validate certificates only with the hosted zone with name servers record information i.e on your domain registrar. In order to request and validate certificates on your destination account. You will first have to change the Name Servers on the Domain Registrar to the values of Name Servers present in the Hosted Zone of Account-B (Dest-account).

Note: As soon as you change the name servers, All the new DNS Queries will be sent to account B (dest-account) as the name servers point to the new Hosted Zone. Please make sure all the resources from account A have been deployed in account B.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
3. Distinguish resources in both accounts using virtual host name selection?
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Since Resources from only one account can be resolved (adhering to the above information). It is not possible to distinguish resources in both accounts using virtual host name selection.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
4. Eventually shutdown the source account and leave the destination account's hosted zones valid (certificates too)?
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

You would like to eventually shutdown the source account. In order to achieve the use case. Please ensure you follow the steps below[2]:

a. Create all the resources in Account B that are used to handle all the request in Account A.

b. Create all the Record Sets present in Hosted Zone of Account A in Hosted Zone of Account B and make sure the records such as CNAME , A (Alias) points to the resources in Account B and not to the resources in Account A. [2]

c. Once you have created the Hosted Zone . Copy the Name Servers of the new Hosted Zone and change it in the Domain Registrar (Go Daddy). This is to make sure that all the new requests reach this hosted zone and do not go the hosted Zone in account A.[1]

d. Once you have changed the Name Servers. You can then create the new Certificates using ACM . This way you will be able to leave the source account and use your destination account without any dependencies.

I sincerely hope and believe that you find this information useful. Please correct me if I misunderstood any part of query here.

In case you have any follow up questions or any other concerns with regards to the information shared above, please feel free to write back to me, it would be my pleasure to assist you further and ushering your issue towards resolution.

Have a Great Day. Stay Safe :)

References:

[1]Changing NameServers on Godaddy:
https://in.godaddy.com/help/change-nameservers-for-my-domains-664


[2]Migrating a hosted zone to a different AWS account:
https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-migrating.html


We value your feedback. Please share your experience by rating this correspondence using the AWS Support Center link at the end of this correspondence. Each correspondence can also be rated by selecting the stars in top right corner of each correspondence within the AWS Support Center.

Best regards,
Syed A.
Amazon Web Services