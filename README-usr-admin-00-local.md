# Creating users in admin member accounts

## To switch to the role for the member account (console)

By default, if you create a member account as part of your organization, AWS automatically creates a role in the account that grants administrator permissions to delegated IAM users in the master account. By default, that role is named ```OrganizationAccountAccessRole```.

When using the role, the user has administrator permissions in the new member account. Instruct your IAM users who are members of the group to do the following to switch to the new role.

* From the upper-right corner of the AWS Organizations console, choose the link that contains the current sign-in name and then choose Switch Role.

* Enter the administrator-provided account ID number and role name.

* For Display Name, enter the text that you want to show on the navigation bar in the upper-right corner in place of your user name while you are using the role. You can optionally choose a color.

* Choose Switch Role. Now all actions that you perform are done with the permissions granted to the role that you switched to. You no longer have the permissions associated with your original IAM user until you switch back.

* When you finish performing actions that require the permissions of the role, you can switch back to your normal IAM user. Choose the role name in the upper-right corner (whatever you specified as the Display Name) and then choose Back to UserName

