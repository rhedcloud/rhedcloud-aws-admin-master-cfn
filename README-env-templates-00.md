# Master template deploy script

**Note**: This step requires the target Admin account has been created.

The README-env-templates-00.sh script sets up the template deployment to the target Admin account, part of the member
account series for the desired environment. A requisite S3 bucket should be created in the target Admin member account,
and is the target S3 bucket referenced from all templates. Three parameters are required for the following S3 bucket:

* CfnTemplateBucket
* BeanstalkAppsBucket
* BeanstalkAppsConfigBucket

Prerequisite buckets in the admin account:

elasticbeanstalk-us-east-1-271882997152


The scripts involved in account creation will perform a number of steps:
1. Using AWS Organizations, a new member account will be created.
1. The account will be moved into the `RHEDcloudAccountAdministrationOrg` OU.
1. Using the [cfn_tool.py](https://bitbucket.org/rhedcloud/rhedcloud-aws-pipeline-scripts/src/master/cfn_tool.py)
   tool, two CloudFormation stacks will be created in the account. 
    1. The first is the [RHEDcloud AWS CloudFormation Template for an account setup](https://bitbucket.org/rhedcloud/rhedcloud-aws-rs-account-cfn/src/master/)
    1. The second is the [RHEDcloud AWS CloudFormation Template for a type 1 VPC](https://bitbucket.org/rhedcloud/rhedcloud-aws-vpc-type1-cfn/src/master/)
1. A SAML provider is created in the account.
1. The account alias is set.
1. Finally, the account is moved into the correct Organizational
   Unit (`RHEDcloudStandardAccountOrg` or `RHEDcloudHipaaAccountOrg`)

There are two scripts that act as a template for account creation.
Like other scripts in this series, a site specific copy of the template will be customized
and executed by the implementation team member.

The `README-env-accounts-00dev.sh` script forms the template for the DEV environment and should
also be the basis for the other three environments.  Be sure to setup the required information
like the IDs of the Organizational Units, access keys, account names and aliases.

The second script to copy and customize is `README-env-accounts-00a.sh`.  At the top of the
file, the `PATH` and `PYTHONPATH` environment variables must be set.  Then the `RHEDcloudIDP`
and `RHEDcloudSamlIssuer` and `template` should be set for the first CFN template.  The second
CFN template requires the VPC CIDR information collected at the beginning of the engagement.

The final step requires manual intervention.  When the AWS Account Service provisions new
accounts, it uses a database sequence to determine the index of the next account.  Because we've
just created three accounts manually, we need to adjust the sequence to take them into account.
The sequence is stored in the `T_SEQUENCE` table in the row `where SEQUENCE_NAME=AccountSequence`
Set the `SEQUENCE_VALUE` column to 3.


## Member Account Maintenance

As previously mentioned, when a new account is provisioned, two CFN templates are run.
Over time, the templates may change in the RHEDcloud GIT repository and you will want to update
the member accounts.

The first step is to deploy the new CFN templates into the S3 buckets.  This is done by running
the deploy-only pipeline `site-aws-rs-account-cfn` or `site-aws-vpc-type1-cfn`.

Then, the `cfn_tool.py` program is used.  Please make sure the `RHEDCLOUD_ASSUME_ROLE` environment
variable is NOT set before running the program.  Some examples of the usage of the program can
be found in the `README-env-accounts-00a.sh` script.

By default, `cfn_tool` uses the `RHEDcloudMaintenanceOperatorRole` in the member account to perform its work.
In order to assume that role, you must be in the same role in the master account which can be
done by using the TKI CLI client.

As an example, we will update the RS Account template in all Standard compliance class accounts.

Using the `cfn_tool`, generate specification files for the accounts in the `RHEDcloudStandardAccountOrg` OU:
* `cfn_tool.py generate existing -r $AWS_DEFAULT_REGION \
    --stack-name rhedcloud-aws-rs-account --ou RHEDcloudStandardAccountOrg \
    --template s3://site-rhedcloud-aws-dev-cfn-templates/rhedcloud-aws-rs-account-cfn.compact.json .`

This creates the spec files in the current directory with names like
`ACCOUNT_ID-AWS_DEFAULT_REGION-rhedcloud-aws-rs-account.json`.

Then the `cfn_tool` is used to update the stacks based on the spec files:
* `cfn_tool.py apply specfile1.json specfile2.json ... specfileN.json`
