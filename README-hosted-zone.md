# Migrating Route 53 Hosted Zones
As part of the account migration from ```rhedcloud-admin-1``` to ```local-rhedcloud-prod-0```, the hosted zone records will migrate to the target account. AWS has a CLI mechanism to do this - see [Migrating a hosted zone to a different AWS account](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-migrating.html) for more information.

The essential information from the source account required for the record "export" is the ```Hosted Zone Id``` which can be found in the Route 53 Hosted Zones console for each hosted zone in the source account - in this case ```rhedcloud-admin-1```.

### Exporting Record Sets

The AWS CLI command to "export" the hosted zone records:

```bash
aws route53 list-resource-record-sets --hosted-zone-id hosted-zone-id > path-to-output-file
``` 

### Converting Record Sets
The output file must be converted for import. A script has been writted using ```jq``` to do this - ```convert-to-upsert.sh```. The essential ```jq``` command:

```bash
jq '.ResourceRecordSets |{"Changes":[.[] |select(.Type!="SOA") |select(.Type!="NS") |{"Action":"CREATE","ResourceRecordSet":.}]}'
```
Note how certain record types are excluded - these cannot be imported, and are part of an *existing* hosted zone. The next section outlines this prerequisite.

### Creating Hosted Zones in the Target Account

Importing record sets requires that the hosted zones exist.  These are created using the Route 53 Console in the target account - in this case ```local-rhedcloud-prod-0```.  There are four zones required:

* **rhedcloud.org.**
* **rhed.cloud.**
* **vpc.rhedcloud.org.**
* **local.**

The **local.** and **vpc.rhedcloud.org.** record sets are private and must be associated with the rhedcloud VPC.

Record each newly created hosted zone's **Hosted Zone ID** for use during import.

**rhedcloud.org.** - Z0035911TV3ICKO03KY2

**rhed.cloud.**    - Z08158153AOYASTH3WKWF

**vpc.rhedcloud.org.** - Z04551603BCYQUEN537UM

**local.** - Z04842072P8DCCELDX2C3 


### Importing Record Sets

Once hosted zones exist in the target account, converted records sets are imported into each zone using AWS CLI and the output file from the "export" above:

```bash
aws route53 change-resource-record-sets --hosted-zone-id id-of-new-hosted-zone --change-batch file://path-to-file-that-contains-records
```

Remember to change credentials for source and target accounts.

## Commands Used for Migration

The commands below should be executed in the ```rhedcloud-local-setup``` directory.  All create json templates are located there.

There are four zones in ```rhedcloud-admin-1```, each containing a unique record set.  So four independent commands are run using json templates containing the corresponding record sets.

```bash
aws route53 change-resource-record-sets --hosted-zone-id Z0035911TV3ICKO03KY2 --change-batch file://zone-records_rhedcloud-admin-1_rhedcloud.org_create.json
aws route53 change-resource-record-sets --hosted-zone-id Z08158153AOYASTH3WKWF --change-batch file://zone-records_rhedcloud-admin-1_rhed.cloud_create.json
aws route53 change-resource-record-sets --hosted-zone-id Z04551603BCYQUEN537UM --change-batch file://zone-records_rhedcloud-admin-1_vpc.rhedcloud.org_create.json
aws route53 change-resource-record-sets --hosted-zone-id Z04842072P8DCCELDX2C3 --change-batch file://zone-records_local.rhedcloud.org_create.json
```

### Results

Note that "INSYNC" change status means changes have propagated to all Route 53 DNS servers.

```bash
$ aws route53 change-resource-record-sets --hosted-zone-id Z0035911TV3ICKO03KY2 --change-batch file://zone-records_rhedcloud-admin-1_rhedcloud.org_create.json
{
    "ChangeInfo": {
        "Id": "/change/C033672624061E9N93Z34",
        "Status": "PENDING",
        "SubmittedAt": "2020-07-13T18:57:44.388Z"
    }
}
$ aws route53 change-resource-record-sets --hosted-zone-id Z08158153AOYASTH3WKWF --change-batch file://zone-records_rhedcloud-admin-1_rhed.cloud_create.json
{
    "ChangeInfo": {
        "Id": "/change/C01184441KEL4T5JUHNZY",
        "SubmittedAt": "2020-07-13T19:00:00.539Z",
        "Status": "PENDING"
    }
}
$ aws route53 change-resource-record-sets --hosted-zone-id Z04551603BCYQUEN537UM --change-batch file://zone-records_rhedcloud-admin-1_vpc.rhedcloud.org_create.json
{
    "ChangeInfo": {
        "SubmittedAt": "2020-07-13T19:00:55.101Z",
        "Status": "PENDING",
        "Id": "/change/C03325052NQLC4Q8AHFH8"
    }
}
$ aws route53 change-resource-record-sets --hosted-zone-id Z04842072P8DCCELDX2C3 --change-batch file://zone-records_local.rhedcloud.org_create.json
{
    "ChangeInfo": {
        "Status": "PENDING",
        "SubmittedAt": "2020-07-13T19:01:46.727Z",
        "Id": "/change/C0965067288Q7E3CNCPV5"
    }
}

...

$ aws route53 get-change --id /change/C033672624061E9N93Z34
{
    "ChangeInfo": {
        "Status": "INSYNC",
        "SubmittedAt": "2020-07-13T18:57:44.388Z",
        "Id": "/change/C033672624061E9N93Z34"
    }
}
$ aws route53 get-change --id /change/C01184441KEL4T5JUHNZY
{
    "ChangeInfo": {
        "Id": "/change/C01184441KEL4T5JUHNZY",
        "SubmittedAt": "2020-07-13T19:00:00.539Z",
        "Status": "INSYNC"
    }
}
$ aws route53 get-change --id /change/C03325052NQLC4Q8AHFH8
{
    "ChangeInfo": {
        "SubmittedAt": "2020-07-13T19:00:55.101Z",
        "Id": "/change/C03325052NQLC4Q8AHFH8",
        "Status": "INSYNC"
    }
}
$ aws route53 get-change --id /change/C0965067288Q7E3CNCPV5
{
    "ChangeInfo": {
        "Id": "/change/C0965067288Q7E3CNCPV5",
        "Status": "INSYNC",
        "SubmittedAt": "2020-07-13T19:01:46.727Z"
    }
}
```

## DNS

You can delegate subdomains to other accounts.

So for example you could create a stage.example.com zone in the stage account. Then, in the dev account, create a NS record for stage.example.com that points to the DNS servers listed for the record in the stage account.

That way you can create entries (i.e www.stage.example.com) via cfn, or whatever other management tools you need. One potential issue is that if you have a wildcard cert for example.com, it will NOT work for hosts that use the subdomain.

Now that I think about it, you could probably set up the top-level domain exactly the same way - that's now you delegate DNS to R53 when you're using other registrars. Set up examplestaging.com zone in the stage account, then update the NS records in the dev account to point to those nameservers.

