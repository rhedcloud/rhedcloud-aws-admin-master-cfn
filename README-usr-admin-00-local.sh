#!/bin/bash

#    To switch to the role for the member account (console)
#
#    When using the role, the user has administrator permissions in the new member account. Instruct your IAM users who are members of the group to do the following to switch to the new role.
#
#    From the upper-right corner of the AWS Organizations console, choose the link that contains the current sign-in name and then choose Switch Role.
#
#    Enter the administrator-provided account ID number and role name.
#
#    For Display Name, enter the text that you want to show on the navigation bar in the upper-right corner in place of your user name while you are using the role. You can optionally choose a color.
#
#    Choose Switch Role. Now all actions that you perform are done with the permissions granted to the role that you switched to. You no longer have the permissions associated with your original IAM user until you switch back.
#
#    When you finish performing actions that require the permissions of the role, you can switch back to your normal IAM user. Choose the role name in the upper-right corner (whatever you specified as the Display Name) and then choose Back to UserName
# User: RHEDcloudOrgMasterSetup


export AWS_ACCESS_KEY_ID=account-access-key
export AWS_SECRET_ACCESS_KEY=account-secret-access-key
export AWS_DEFAULT_REGION=us-east-1

#  create-user
#[--path <value>]
#--user-name <value>
#[--permissions-boundary <value>]
#[--tags <value>]
#[--cli-input-json <value>]
#[--generate-cli-skeleton <value>]

# aws iam create-user --user-name bitbucket-deploy

# Output:

#    {
#        "User": {
#            "UserName": "bitbucket-deploy",
#            "Arn": "arn:aws:iam::784619214844:user/bitbucket-deploy",
#            "CreateDate": "2020-06-26T17:31:48Z",
#            "Path": "/",
#            "UserId": "AIDA3NLXTC76JBH2RUXP4"
#        }
#    }


#create-login-profile
#--user-name <value>
#--password <value>
#[--password-reset-required | --no-password-reset-required]
#[--cli-input-json <value>]
#[--generate-cli-skeleton <value>]

aws iam create-login-profile --user-name sbrodeur --password Tisbpw_p_2.1 --no-password-reset-required
