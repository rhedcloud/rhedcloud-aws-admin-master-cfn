#!/bin/bash

echo copy/edit this template script to README-env-accounts-01-site.sh
echo chmod u+x README-env-accounts-01-site.sh
echo mkdir rhedcloud-site-setup
echo cd rhedcloud-site-setup
echo ../README-env-accounts-01-site.sh 2>&1 \| tee -a README-env-accounts-01-site.log
#exit

# This script just outputs SQL statements that should be verified and run in the correct database

command -v aws >/dev/null 2>&1 || { echo >&2 "AWS cli tool (aws) required but it's not installed.  Aborting."; exit 1; }
command -v jq >/dev/null 2>&1 || { echo >&2 "jq commandline JSON processor (jq) required but it's not installed.  Aborting."; exit 1; }

### Start of Variables to edit ###

# AWS Region to operate in
export AWS_DEFAULT_REGION=us-east-1

# ENVIRONMENTS is an array of Environments to loop through
ENVIRONMENTS=(
  "DEV"
  "TEST"
  "STAGE"
  "PROD"
)

### End of Variables to edit ###


for E in "${ENVIRONMENTS[@]}" ; do
    Elc=$(echo $E | tr '[:upper:]' '[:lower:]')
    Ecc="$(tr '[:lower:]' '[:upper:]' <<< ${Elc:0:1})${Elc:1}"

    case $E in
      DEV)
        MASTER_ACT=dev_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=dev_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=dev_aws_secret_access_key_goes_here
        ;;
      TEST)
        MASTER_ACT=test_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=test_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=test_aws_secret_access_key_goes_here
        ;;
      STAGE)
        MASTER_ACT=stage_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=stage_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=stage_aws_secret_access_key_goes_here
        ;;
      PROD)
        MASTER_ACT=prod_aws_account_id_goes_here
        export AWS_ACCESS_KEY_ID=prod_aws_access_key_id_goes_here
        export AWS_SECRET_ACCESS_KEY=prod_aws_secret_access_key_goes_here
        ;;
    esac

    ###############
    ##### Account metadata for base accounts

    # Could use AWS Account Service to create the account metadata by sending Create-Request.xml using the activemq console
    # This step happens much later when the service is up and running

    # Or, and what we're showing here, directly insert the account data
    # This implies that AWS Account Service has been started at least once to create the tables
    # This script just outputs SQL statements that should be verified and run in the correct database


    list_accounts_sample='
{
    "Accounts": [
        {
            "Id": "123456789012",
            "Arn": "arn:aws:organizations::123456789012:account/o-a1b2c3d4e5/123456789012",
            "Email": "aws-dev-master@site.org",
            "Name": "AWS Dev Master",
            "Status": "ACTIVE",
            "JoinedMethod": "INVITED",
            "JoinedTimestamp": 1524335870.297
        },
        {
            "Id": "345678901234",
            "Arn": "arn:aws:organizations::123456789012:account/o-a1b2c3d4e5/345678901234",
            "Email": "aws-dev-1@site.org",
            "Name": "Site Dev 1",
            "Status": "ACTIVE",
            "JoinedMethod": "CREATED",
            "JoinedTimestamp": 1526584493.392
        },
        {
            "Id": "567890123456",
            "Arn": "arn:aws:organizations::123456789012:account/o-a1b2c3d4e5/567890123456",
            "Email": "aws-dev-2@site.org",
            "Name": "Site Dev 2",
            "Status": "ACTIVE",
            "JoinedMethod": "CREATED",
            "JoinedTimestamp": 1526584645.005
        }
    ]
}
'

    cat <<EOF
-- $E $MASTER_ACT begin
-- TODO - before running, set COMPLIANCE_CLASS to 'Standard' or 'HIPAA'
-- TODO - uncomment 'srdExemptBecause' for master account
EOF

    list_accounts=$list_accounts_sample
#    list_accounts=$(aws organizations list-accounts)

    for row in $(echo "${list_accounts}" | jq -r '.Accounts[] | @base64'); do
        _jq() {
         echo ${row} | base64 --decode | jq -r ${1}
        }
        cat <<EOF

-- $E $(_jq '.Id') arn $(_jq '.Arn') status $(_jq '.Status')
insert into account (account_id, account_name, compliance_class, password_location, account_owner_id, financial_account_number, create_user, create_datetime, last_update_user, last_update_datetime)
   values ('$(_jq '.Id')', '$(_jq '.Name')', 'fixme', 'AWS default', 'P9999999', '9999999999', 'P9999999', current_timestamp, null, null);
insert into account_email_address (account_id, type_, email) values ('$(_jq '.Id')', 'primary',    '$(_jq '.Email')');
insert into account_email_address (account_id, type_, email) values ('$(_jq '.Id')', 'operations', '$(_jq '.Email')');
insert into account_property (account_id, key_, value) values ('$(_jq '.Id')', 'srdExempt', 'true');
-- insert into account_property (account_id, key_, value) values ('$(_jq '.Id')', 'srdExemptBecause', '$E Master Must Remain Exempt');
EOF

    done

    cat <<EOF
-- $E $MASTER_ACT end


EOF

done
